import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from './_services/script-loader.service';
import { ThemeRoutingModule } from './theme/theme-routing.module';
import { AuthModule } from './auth/auth.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './_services/interceptors/auth.service';
import { AgGridModule } from 'ag-grid-angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwtService } from './auth/_services/jwt.service';
import { UserService } from './auth/_services/user.service';

@NgModule({
    declarations: [ThemeComponent, AppComponent],
    imports: [
        LayoutModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        ThemeRoutingModule,
        AuthModule,
        AgGridModule.withComponents([]),
        NgbModule
    ],
    providers: [
        ScriptLoaderService,
        UserService,
        JwtService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthService, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
