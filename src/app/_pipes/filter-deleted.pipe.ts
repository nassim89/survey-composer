import { Pipe, PipeTransform } from '@angular/core';
import { AbstractEntity } from '../models/abstract/abstract-entity';

@Pipe({
    name: 'filterDeleted',
    pure: false
})
export class FilterDeletedPipe implements PipeTransform {

    transform(entities: AbstractEntity[]) {
        return entities.filter(e => !e.isDeleted);
    }

}
