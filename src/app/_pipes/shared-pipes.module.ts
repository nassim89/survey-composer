import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterDeletedPipe } from './filter-deleted.pipe';

@NgModule({
    imports: [CommonModule],
    declarations: [FilterDeletedPipe],
    exports: [FilterDeletedPipe]
})
export class SharedPipesModule { }