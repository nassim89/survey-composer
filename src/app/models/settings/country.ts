import { AbstractEntity } from '../abstract/abstract-entity';
import { Department } from './department';
import { Participant } from '../participants/participant';
import { Survey } from '../surveys/survey';

export class Country extends AbstractEntity {
    countryId: number;
    countryCode: string;
    descripiton: string;
    departments?: Set<Department>;
    participants?: Set<Participant>;
    surveys?: Set<Survey>;
}
