import { AbstractEntity } from '../abstract/abstract-entity';

export class Language extends AbstractEntity {
    languageId: number;
    languageCode: string;
    isDefault: boolean;
    description: string;

    constructor() {
        super();
        this.languageCode = '';
        this.isDefault = false;
        this.description = '';
    }
}
