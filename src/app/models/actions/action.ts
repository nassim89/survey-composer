import { PlatformUser } from '../users/platform-user';
import { ActionType } from '../enums/action-type.enum';

export class Action {
    actionId: number;
    actionType: ActionType;
    dateOfAction: Date;
    entityId: number;
    entityClass: string;
    actorUser: PlatformUser;
}
