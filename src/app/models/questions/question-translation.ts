import { AbstractEntity } from '../abstract/abstract-entity';
import { Language } from '../settings/language';

export class QuestionTranslation extends AbstractEntity {
    questionTranslationId: number;
    text: string;
    translationVersion: number;
    translationComment: string;
    language: Language;
}
