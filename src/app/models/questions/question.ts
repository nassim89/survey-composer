import { AbstractEntity } from '../abstract/abstract-entity';
import { QuestionVersion } from './question-version';
import { Label } from './label';

export class Question extends AbstractEntity {
    questionId: number;
    versions?: QuestionVersion[];
    labels?: Label[];
    description: string;
}
