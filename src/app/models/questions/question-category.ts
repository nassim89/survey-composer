import { AbstractCategory } from '../abstract/abstract-category';

export class QuestionCategory extends AbstractCategory {
    questionCategoryId: number;
    children?: QuestionCategory[];
}
