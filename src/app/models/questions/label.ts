import { AbstractEntity } from '../abstract/abstract-entity';

export class Label extends AbstractEntity {
    labelId: number;
    labelText: string;
    description: string;
}
