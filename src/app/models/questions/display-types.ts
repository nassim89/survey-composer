export class DisplayType {
    id: number;
    type: string;

    constructor(id?: number, type?: string) {
        this.id = id;
        this.type = type;
    }

    getDisplayTypes(): DisplayType[] {
        const dt = new Array<DisplayType>();
        dt.push(new DisplayType(0, 'SLIDER'));
        dt.push(new DisplayType(1, 'CHECKBOX'));
        dt.push(new DisplayType(2, 'RADIOBUTTON'));
        dt.push(new DisplayType(3, 'DROPDOWN'));
        dt.push(new DisplayType(4, 'TEXTBOX'));
        dt.push(new DisplayType(6, 'DNDRANK'));
        return dt;
    }
}

/*
    SLIDER,
    CHECKBOX,
    RADIOBUTTON,
    DROPDOWN,
    TEXTBOX,
    PAGE,
    GROUP
*/
