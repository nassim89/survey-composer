import { AbstractEntity } from '../abstract/abstract-entity';
import { QuestionTranslation } from './question-translation';

export class QuestionVersion extends AbstractEntity {
    questionVersionId: number;
    version: number;
    translations?: QuestionTranslation[];
}
