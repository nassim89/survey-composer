import { DisplayType } from '../questions/display-types';

export const DISPLAY_TYPES = new DisplayType().getDisplayTypes();
