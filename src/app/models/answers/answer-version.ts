import { AbstractEntity } from '../abstract/abstract-entity';
import { AnswerTranslation } from './answer-translation';

export class AnswerVersion extends AbstractEntity {
    answerVersionId: number;
    version: number;
    translations?: AnswerTranslation[];
}
