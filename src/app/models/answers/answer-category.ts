import { AbstractEntity } from '../abstract/abstract-entity';

export class AnswerCategory extends AbstractEntity {
    answerCategoryId: number;
    children?: Set<AnswerCategory>;
}
