import { AbstractEntity } from '../abstract/abstract-entity';
import { AnswerVersion } from './answer-version';

export class Answer extends AbstractEntity {
    answerId: number;
    description: string;
    version?: AnswerVersion[];
}
