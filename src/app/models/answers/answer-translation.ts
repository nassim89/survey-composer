import { AbstractEntity } from '../abstract/abstract-entity';
import { Language } from '../settings/language';

export class AnswerTranslation extends AbstractEntity {
    answerTranslationId: number;
    text: string;
    translationComment: string;
    translationVersion: number;
    language: Language;
}
