import { QuestionTranslationDTO } from '../../dto/questions/output/question-translation-dto';
import { AnswerTranslationDTO } from '../../dto/answers/output/answer-translation-dto';
import { CreateQAMap } from '../../dto/questionnaires/input/create-qamap';

export class QA {
    private _question?: QuestionTranslationDTO;
    private _answers?: AnswerTranslationDTO[];

    constructor(question?: QuestionTranslationDTO) {
        this._question = question;
        this._answers = new Array<AnswerTranslationDTO>();
    }

    get question(): QuestionTranslationDTO {
        return this._question;
    }

    set question(question: QuestionTranslationDTO) {
        this._question = question;
    }

    get answers(): AnswerTranslationDTO[] {
        return this._answers;
    }

    set answers(answers: AnswerTranslationDTO[]) {
        this._answers = answers;
    }

    addAnswer(answer: AnswerTranslationDTO) {
        if (this._answers.find(a => a.answerTranslationId === answer.answerTranslationId) === undefined) {
            this._answers = [...this._answers, answer];
        }
    }

}
