import { AbstractEntity } from '../abstract/abstract-entity';
import { QuestionTranslation } from '../questions/question-translation';
import { AnswerTranslation } from '../answers/answer-translation';

export class QAmap extends AbstractEntity {
    mapId: number;
    mappingTag: string;
    questionTranslation: QuestionTranslation;
    answerTranslation: AnswerTranslation;
}
