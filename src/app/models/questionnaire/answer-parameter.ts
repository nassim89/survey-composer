import { AbstractEntity } from '../abstract/abstract-entity';

export class AnswerParameter extends AbstractEntity {
    apOther: boolean;
    apRequireOther: boolean;
    apNa: boolean;
    apNone: boolean;
    apAll: boolean;
    apFixed: boolean;
}
