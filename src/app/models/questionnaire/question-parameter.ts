import { AbstractEntity } from '../abstract/abstract-entity';

export class QuestionParameter extends AbstractEntity {
    qpOrientation: string;
    qpLabelsRight: boolean;
    qpQuestionDescriptionAbove: boolean;
    qpCustomCSS: string;
    qpForceNumeric: boolean;
    qpForcePercent: boolean;
    qpForceCurrency: boolean;
    qpMinNumber: number;
    qpMaxNumber: number;
    qpMinAnswerPerRow: number;
    qpMinimumResponse: number;
}
