import { AbstractEntity } from '../abstract/abstract-entity';
import { TitleTranslation } from './title-translation';

export class Title extends AbstractEntity {
    titleId: number;
    defaultTitleText: string;
    titleTranslations: TitleTranslation[];

    constructor() {
        super();
        this.defaultTitleText = '';
        this.titleTranslations = new Array<TitleTranslation>();
    }
}
