import { AbstractEntity } from '../abstract/abstract-entity';
import { Language } from '../settings/language';

export class TitleTranslation extends AbstractEntity {
    titleTranslationId: number;
    titleText: string;
    language: Language;

    constructor() {
        super();
        this.titleText = '';
        this.language = new Language();
    }
}
