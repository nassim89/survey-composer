import { AbstractEntity } from '../abstract/abstract-entity';
import { Title } from './title';
import { Section } from './section';

export class Questionnaire extends AbstractEntity {
    questionnaireId: number;
    title: Title;
    sections: Section[];

    constructor() {
        super();
        this.title = new Title();
        this.sections = new Array<Section>();
    }
}
