import { AbstractEntity } from '../abstract/abstract-entity';
import { Title } from './title';
import { QAmap } from './qa-map';

export class Section extends AbstractEntity {
    sectionId: number;
    sectionTitle: Title;
    qaMaps: Set<QAmap>;
    childrenSection: Set<Section>;
}
