import { PermissionEntity } from './permission-entity';
import { AbstractEntity } from '../abstract/abstract-entity';

export class UserRole extends AbstractEntity {
    userRoleId: number;
    name: string;
    permission: PermissionEntity[];
}
