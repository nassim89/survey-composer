import { AbstractEntity } from '../abstract/abstract-entity';

export class PermissionEntity extends AbstractEntity {
    permissionId: number;
    name: string;
    description: string;
}
