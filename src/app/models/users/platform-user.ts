import { AbstractEntity } from '../abstract/abstract-entity';
import { UserRole } from './user-role';

export class PlatformUser extends AbstractEntity {
    userId: number;
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    roles: UserRole[];

}
