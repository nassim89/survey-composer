export enum ActionType {
    CREATE,
    MODIFY,
    DELETE,
    READ,
    LOGIN,
    LOGOUT,
    SET_DEFAULT_SETTINGS,
    DROP_DEFAULT_SETTINGS
}
