import { AbstractEntity } from './abstract-entity';

export class AbstractCategory extends AbstractEntity {
    categoryName: string;
    description: string;
}
