import { AbstractEntity } from './abstract-entity';

export class AbstractGroup extends AbstractEntity {
    groupName: string;
    description: string;
}
