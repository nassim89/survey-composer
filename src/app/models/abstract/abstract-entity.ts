export class AbstractEntity {
    creationDate: Date;
    lastModificationDate: Date;
    isDeleted: false;
    deletedOn: Date;

    constructor() {
        this.creationDate = new Date();
        this.lastModificationDate = new Date();
        this.isDeleted = false;
        this.deletedOn = null;
    }
}
