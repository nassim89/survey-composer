export class QuestionGroup {
    public parentGroupId: number = null;
    public creationDate: Date = null;
    public groupName: string = '';
    public description: string = '';
    public deletedOn: Date = null;
    public lastModificationDate: Date = null;
    public questionGroupId: number = null;
    public children: QuestionGroup[] = [];
    public isDeleted: boolean = false;
}
