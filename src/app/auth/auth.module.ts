import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.routing';
import { AuthComponent } from './auth.component';
import { AlertComponent } from './_directives/alert.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './_guards/auth.guard';
import { NoAuthGuard } from './_guards/no-auth.guard';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [AuthComponent, AlertComponent, LogoutComponent],
    imports: [CommonModule, FormsModule, HttpClientModule, AuthRoutingModule],
    providers: [AuthGuard, NoAuthGuard, AlertService, UserService],
    entryComponents: [AlertComponent]
})
export class AuthModule {}
