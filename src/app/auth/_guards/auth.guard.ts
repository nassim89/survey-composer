import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../_services/user.service';
import { JwtService } from '../_services/jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router, private userService: UserService, private jwtService: JwtService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        if (this.jwtService.getCurrentUser()) {
            return Observable.of(true);
        } else {
            this.router.navigate(['/login']);
            return Observable.of(false);
        }
    }
}
