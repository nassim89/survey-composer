import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../_services/user.service';
import { take, map, tap } from 'rxjs/operators';

@Injectable()
export class NoAuthGuard implements CanActivate {
    constructor(private router: Router, private userService: UserService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        // let result = this.userService.isAuthenticated.pipe(
        //     take(1),
        //     tap(isAuth => console.log(isAuth)),
        //     map(isAuth => !isAuth)
        // );
        // console.log(result);
        return Observable.of(true);
    }
}
