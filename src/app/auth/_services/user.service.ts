import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';

import { JwtService } from './jwt.service';
import { map, distinctUntilChanged } from 'rxjs/operators';
import { PlatformUserDTO } from '../../dto/users/output/platform-user-dto';

@Injectable()
export class UserService {
    private currentUserSubject = new BehaviorSubject<PlatformUserDTO>({} as PlatformUserDTO);
    public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();

    constructor(private http: HttpClient, private jwtService: JwtService) {}

    // Verify JWT in localstorage with server & load user's info.
    // This runs once on application startup.
    populate() {
        // If JWT detected, attempt to get & store user's info
        if (this.jwtService.getToken()) {

        } else {
            // Remove any potential remnants of previous auth states
            this.purgeAuth();
        }
    }

    setAuth(user: PlatformUserDTO) {
        // Save JWT sent from server in localstorage
        this.jwtService.saveToken(user.accessToken);
        this.jwtService.saveCurrentUser(user);
        // Set current user data into observable
        this.currentUserSubject.next(user);
        // Set isAuthenticated to true
        this.isAuthenticatedSubject.next(true);

    }

    purgeAuth() {
        // Remove JWT from localstorage
        this.jwtService.destroyToken();
        this.jwtService.destroyCurrentUser();
        // Set current user to an empty object
        this.currentUserSubject.next({} as PlatformUserDTO);
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
    }

    attemptAuth(userName: string, password: string): Observable<PlatformUserDTO> {
        const route = 'http://109.135.0.135/survey-composer/users/login';
        return this.http
            .post<PlatformUserDTO>(route, { userName: userName, password: password })
            .pipe(
                map(data => {
                    this.setAuth(data);
                    return data;
                })
            );
    }

    getCurrentUser(): PlatformUserDTO {
        return this.currentUserSubject.value;
    }
}
