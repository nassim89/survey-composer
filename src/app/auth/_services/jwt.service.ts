import { Injectable } from '@angular/core';
import { PlatformUserDTO } from '../../dto/users/output/platform-user-dto';

@Injectable()
export class JwtService {
    getToken(): String {
        return window.localStorage['jwtToken'];
    }

    saveToken(token: String) {
        window.localStorage['jwtToken'] = token;
    }

    destroyToken() {
        window.localStorage.removeItem('jwtToken');
    }

    getCurrentUser(): PlatformUserDTO {
        if (window.localStorage['currentUser']) {
            return JSON.parse(window.localStorage['currentUser']);
        } else {
            return null;
        }
    }

    saveCurrentUser(user: PlatformUserDTO) {
        window.localStorage['currentUser'] = JSON.stringify(user);
    }

    destroyCurrentUser() {
        window.localStorage.removeItem('currentUser');
    }
}
