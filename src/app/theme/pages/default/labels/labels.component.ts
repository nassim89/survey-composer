import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { LabelDTO } from '../../../../dto/questions/output/label-dto';
import { UpdateLabel } from '../../../../dto/questions/input/update-label';
import { SettingsService } from '../../../../_services/settings.service';
import { CreateLabel } from '../../../../dto/questions/input/create-label';
import { QuestionsService } from '../../../../_services/questions.service';

@Component({
    selector: 'app-labels',
    templateUrl: './labels.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./labels.component.css']
})

export class LabelsComponent implements OnInit, AfterViewInit {
    labels: LabelDTO[];
    gridApi;
    gridColumnApi;
    rowData: any[];
    columnDefs: any[];
    selectedLabel: LabelDTO;
    multiSortKey: string;
    createSubmitted = false;
    updateSubmitted = false;
    paginationPageSize = 15;
    updateLabel: UpdateLabel;
    addLabelForm: FormGroup;
    updateLabelForm: FormGroup;
    deleteLabelForm: FormGroup;

    constructor(private _scriptLoader: ScriptLoaderService,
        private _settingsService: SettingsService,
        private _questionsService: QuestionsService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {

        this.selectedLabel = new LabelDTO();

        this.addLabelForm = this.formBuilder.group({
            label: ['', Validators.required],
            description: ['']
        });

        this.updateLabelForm = this.formBuilder.group({
            label: ['', Validators.required],
            description: [''],
            labelSelect: []
        });

        this.deleteLabelForm = this.formBuilder.group({
            labelSelect: [],
            ckWantToRemove: [false, Validators.required]
        })

        this.updateLabel = new UpdateLabel('', '');
        this.columnDefs = this.getColumnDefs();
        this.multiSortKey = 'ctrl';
    }

    ngAfterViewInit() {
    }

    // TODO to be eventually defined directly from the categories
    getColumnDefs(): any[] {
        return [
            { headerName: 'Label', field: 'labelText', cellStyle: { 'font-weight': 'bold' }, filter: 'agTextColumnFilter' },
            { headerName: 'Description', field: 'description', filter: 'agTextColumnFilter' }
        ];
    }


    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this._questionsService.findLabels().subscribe(
            labels => {
                this.labels = labels.filter(l => l.isDeleted === false);
                this.rowData = this.labels;
                params.api.sizeColumnsToFit();
            }
        )

    }

    fetchLabelAndRefresh() {
        this._questionsService.findLabels().subscribe(
            labels => {
                this.labels = labels.filter(l => l.isDeleted === false);
                this.rowData = this.labels;
                this.gridApi.refreshCells();
                this.gridApi.sizeColumnsToFit();
            }
        )
    }

    onRowClicked(event: any) {
        if (event && event.data) {
            this.updateLabel.label = event.data.labelText;
            this.updateLabel.description = event.data.description;
            this.selectedLabel = event.data;
        }
    }

    onCreate() {
        if (this.addLabelForm.invalid) {
            return;
        } else {
            let label: string = this.addLabelForm.value.label;

            let description: string = this.addLabelForm.value.description;

            const createLang = new CreateLabel(label, description);

            this._questionsService.createLabel(createLang).subscribe(
                label => {
                    if (label !== undefined) {
                        this.fetchLabelAndRefresh();
                        this.selectedLabel = label;
                    }
                },
                error => {
                    console.log('Error while retrieving labels.')
                }
            );
        }
    }

    onSelectToUpdateChange(event) {
        this.selectedLabel = event;
        if (this.selectedLabel) {
            this.updateLabel.label = this.selectedLabel.labelText;
            this.updateLabel.description = this.selectedLabel.description;
        }
    }

    onUpdate() {
        if (this.updateLabelForm.invalid) {
            return;
        } else {
            let label: string = this.updateLabelForm.value.label;

            let description: string = this.updateLabelForm.value.description;

            const updateLang = new UpdateLabel(label, description);

            this._questionsService.updateLabel(this.selectedLabel.labelId, updateLang).subscribe(
                label => {
                    this.fetchLabelAndRefresh();
                    this.selectedLabel = label;
                },
                error => {
                    console.log('Error while retrieving labels.');
                    console.log(error);
                }
            );
        }
    }

    onDelete() {
        if (this.deleteLabelForm.invalid) {
            return;
        } else {
            let wantToRemove: boolean = this.deleteLabelForm.value.ckWantToRemove;
            if (wantToRemove) {
                this._questionsService.deleteLabel(this.selectedLabel.labelId).subscribe(
                    cat => {
                        this.fetchLabelAndRefresh();
                        this.deleteLabelForm.reset();
                    },
                    error => {
                        console.log('Error while deleting label.');
                        console.log(error);
                    }
                )
            }
        }
    }
}