import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { LabelsComponent } from './labels.component';
import { SettingsService } from '../../../../_services/settings.service';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { AgGridModule } from '../../../../../../node_modules/ag-grid-angular';
import { NgSelectModule } from '../../../../../../node_modules/@ng-select/ng-select';
import { SharedPipesModule } from '../../../../_pipes/shared-pipes.module';
import { MessageService } from '../../../../_services/message.service';
import { QuestionsService } from '../../../../_services/questions.service';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': LabelsComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([]),
        NgSelectModule,
        SharedPipesModule
    ], exports: [
        RouterModule,
    ], declarations: [
        LabelsComponent,
    ], providers: [
        SettingsService, QuestionsService, MessageService
    ]
})
export class LabelsModule {
}