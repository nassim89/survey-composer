import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { SectionDTO } from '../../../../dto/questionnaires/output/section-dto';

@Component({
    selector: 'app-read-only-page',
    templateUrl: 'read-only-page.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./read-only-page.component.css']
})
export class ReadOnlyPageComponent implements OnInit {
    _page: SectionDTO;

    constructor() { }

    ngOnInit() { }

    @Input()
    set page(page: SectionDTO) {
        this._page = page;
    }

    get page(): SectionDTO {
        return this._page;
    }
}
