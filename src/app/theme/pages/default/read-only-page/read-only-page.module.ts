import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadOnlyPageComponent } from './read-only-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuestionsService } from '../../../../_services/questions.service';
import { SettingsService } from '../../../../_services/settings.service';
import { MessageService } from '../../../../_services/message.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { AnswersService } from '../../../../_services/answers.service';
import { ReadOnlyQuestionDisplayModule } from '../read-only-question-display/read-only-question-display.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        ReadOnlyQuestionDisplayModule
    ],
    exports: [ReadOnlyPageComponent],
    declarations: [ReadOnlyPageComponent],
    providers: [QuestionsService, AnswersService, SettingsService, MessageService]
})
export class ReadOnlyPageModule { }
