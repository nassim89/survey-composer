import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { AnswersComponent } from './answers.component';
import { AnswersService } from '../../../../_services/answers.service';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { NgSelectModule } from '../../../../../../node_modules/@ng-select/ng-select';
import { MessageService } from '../../../../_services/message.service';
import { SettingsService } from '../../../../_services/settings.service';
import { SharedPipesModule } from '../../../../_pipes/shared-pipes.module';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': AnswersComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        SharedPipesModule
    ], exports: [
        RouterModule,
    ], declarations: [
        AnswersComponent,
    ], providers: [
        AnswersService, SettingsService, MessageService
    ]
})
export class AnswersModule {
}