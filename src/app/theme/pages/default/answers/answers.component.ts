import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { AnswersService } from '../../../../_services/answers.service';
import { AnswerCategoryDTO } from '../../../../dto/answers/output/answer-category-dto';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { AnswerDTO } from '../../../../dto/answers/output/answer-dto';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';
import { CreateAnswer } from '../../../../dto/answers/input/create-answer';
import { AnswerVersionDTO } from '../../../../dto/answers/output/answer-version-dto';
import { CreateAnswerVersion } from '../../../../dto/answers/input/create-answer-version';
import { AnswerTranslationDTO } from '../../../../dto/answers/output/answer-translation-dto';
import { CreateAnswerTranslation } from '../../../../dto/answers/input/create-answer-translation';
import { LanguageDTO } from '../../../../dto/settings/output/language-dto';
import { UpdateAnswerTranslation } from '../../../../dto/answers/input/update-answer-translation';
import { SettingsService } from '../../../../_services/settings.service';
import { ActionType } from '../../../../models/enums/action-type.enum';

@Component({
    selector: 'app-answers',
    templateUrl: './answers.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit, AfterViewInit {
    category: AnswerCategoryDTO;
    answers: AnswerDTO[];
    languages: LanguageDTO[];
    allButEnglish: LanguageDTO[];
    onlyEnglish: LanguageDTO[];
    answerCategoryId: number;
    addAnswerFormGroup: FormGroup;
    addVersionFormGroup: FormGroup;
    addTranslationFormGroup: FormGroup;
    removeVersionFormGroup: FormGroup;
    editTranslationFormGroup: FormGroup;
    removeTranslationFormGroup: FormGroup;
    addAnswerSubmitted: boolean = false;
    currentAnswer: AnswerDTO;
    currentVersion: AnswerVersionDTO;
    currentTranslation: AnswerTranslationDTO;
    enumAction = ActionType;
    activeTab: number = 0;
    addAnswerPressed = false;
    addVersionPressed = false;
    removeVersionPressed = false;
    addTranslationPressed = false;
    editTranslationPressed = false;
    removeTranslationPressed = false;

    constructor(
        private route: ActivatedRoute,
        private _scriptLoader: ScriptLoaderService,
        private _answersService: AnswersService,
        private _settingsService: SettingsService,
        private formBuilder: FormBuilder
    ) {
        this.addAnswerFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            description: [''],
            comment: ['']
        });

        this.addVersionFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            comment: ['']
        });

        this.removeVersionFormGroup = this.formBuilder.group({
            ckContentCheck: [false, Validators.required],
            ckWantToRemove: [false, Validators.required]
        });

        this.addTranslationFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            language: [null, Validators.required],
            comment: ['']
        });

        this.editTranslationFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            language: [null, Validators.required],
            comment: ['']
        });

        this.removeTranslationFormGroup = this.formBuilder.group({
            ckContentCheck: [false, Validators.required],
            ckWantToRemove: [false, Validators.required]
        });
    }

    ngOnInit() {
        // Get the answerCategoryId
        this.answerCategoryId = this.route.snapshot.params.answerCategoryId;
        // Get the category from the answerCategoryId
        this._answersService.findAnswerCategoriesById(this.answerCategoryId).subscribe(
            cat => {
                this.category = cat;
            },
            error => {
                console.log('Retrieving the category failed.');
                console.log(error);
            }
        );
        // Get the languages
        this._settingsService.findLanguages().subscribe(
            languages => {
                this.languages = languages;
                this.allButEnglish = this.languages
                    .filter(l => !l.isDefault)
                    .filter(l => !l.isDeleted);
                this.onlyEnglish = this.languages
                    .filter(l => l.isDefault)
                    .filter(l => !l.isDeleted);
            },
            error => {
                console.log('Retrieving the languages failed.');
                console.log(error);
            }
        );

        // Get the answers from the answerCategoryId
        this._answersService.findAnswers(this.answerCategoryId).subscribe(
            answers => {
                this.answers = answers;
                this.answers.forEach(a => {
                    a.activeTab = 0;
                });
            },
            error => {
                console.log('Retrieving the answers failed.');
                console.log(error);
            }
        );
    }

    ngAfterViewInit() {}

    addAnswer() {
        this.addAnswerPressed = true;
        if (this.addAnswerFormGroup.invalid) {
            this.addAnswerPressed = false;
            return;
        } else {
            let text: string = this.addAnswerFormGroup.get('text').value;
            const description = this.addAnswerFormGroup.get('description').value;
            const comment = this.addAnswerFormGroup.get('comment').value;

            const input = new CreateAnswer(text, this.answerCategoryId, description, comment);

            this._answersService.createAnswer(input).subscribe(
                answer => {
                    answer.activeTab = 0;
                    this.answers.unshift(answer);
                    this.addAnswerFormGroup.reset();
                    this.addAnswerPressed = false;
                },
                error => {
                    console.log(error);
                    this.addAnswerPressed = false;
                }
            );
        }
    }

    addVersion() {
        this.addVersionPressed = true;
        if (this.addVersionFormGroup.invalid) {
            this.addVersionPressed = false;
            return;
        } else {
            let text: string = this.addVersionFormGroup.get('text').value;
            const comment = this.addVersionFormGroup.get('comment').value;

            const input = new CreateAnswerVersion(this.currentAnswer.answerId, text, comment);

            this._answersService.createVersion(this.currentAnswer.answerId, input).subscribe(
                version => {
                    this.currentAnswer.versions.unshift(version);
                    this.currentAnswer.activeTab = 0;
                    this.addVersionFormGroup.reset();
                    this.addVersionPressed = false;
                },
                error => {
                    this.addVersionPressed = false;
                    console.log(error);
                }
            );
        }
    }

    removeVersion() {
        this.removeVersionPressed = true;
        if (this.removeVersionFormGroup.invalid) {
            this.removeVersionPressed = false;
            return;
        } else {
            const answerId = this.currentAnswer.answerId;
            const versionId = this.currentVersion.answerVersionId;
            const contentCheck: boolean = this.removeVersionFormGroup.value.ckContentCheck;
            const wantToRemove: boolean = this.removeVersionFormGroup.value.ckWantToRemove;
            if (contentCheck && wantToRemove) {
                const count = this.currentAnswer.versions.filter(version => !version.isDeleted)
                    .length;
                if (count < 2) {
                    this._answersService.deleteAnswer(answerId).subscribe(
                        answer => {
                            const answerToRemove = this.answers.find(
                                elem => elem.answerId === answer.answerId
                            );
                            const index = this.answers.indexOf(answerToRemove);
                            this.answers[index] = answer;
                            this.removeVersionFormGroup.reset();
                            this.removeVersionPressed = false;
                        },
                        error => {
                            this.removeVersionPressed = false;
                            console.log('error while deleting this answer', error);
                        }
                    );
                } else {
                    this._answersService.deleteAnswerVersion(answerId, versionId).subscribe(
                        version => {
                            const versionToRemove = this.currentAnswer.versions.find(
                                elem => elem.answerVersionId === version.answerVersionId
                            );
                            const index = this.currentAnswer.versions.indexOf(versionToRemove);
                            this.currentAnswer.versions[index] = version;
                            this.currentAnswer.activeTab = 0;
                            this.removeVersionFormGroup.reset();
                            this.removeVersionPressed = false;
                        },
                        error => {
                            console.log(error);
                            this.removeVersionPressed = false;
                        }
                    );
                }
            } else {
                this.removeVersionPressed = false;
            }
        }
    }

    addTranslation() {
        this.addTranslationPressed = true;
        if (this.addTranslationFormGroup.invalid) {
            this.addTranslationPressed = false;
            return;
        } else {
            const answerId = this.currentAnswer.answerId;
            const versionId = this.currentVersion.answerVersionId;
            const text: string = this.addTranslationFormGroup.get('text').value;
            const languageId: number = this.addTranslationFormGroup.get('language').value;
            const comment = this.addTranslationFormGroup.get('comment').value;
            const input = new CreateAnswerTranslation(versionId, text, languageId, comment);

            this._answersService.createAnswerTranslation(answerId, versionId, input).subscribe(
                translation => {
                    this.currentVersion.translations.unshift(translation);
                    this.addTranslationFormGroup.reset();
                    this.addTranslationPressed = false;
                },
                error => {
                    console.log(error);
                    this.addTranslationPressed = false;
                }
            );
        }
    }

    editTranslation() {
        this.editTranslationPressed = true;
        if (this.editTranslationFormGroup.invalid) {
            this.editTranslationPressed = false;
            return;
        } else {
            const answerId = this.currentAnswer.answerId;
            const versionId = this.currentVersion.answerVersionId;
            const translationId = this.currentTranslation.answerTranslationId;
            const text = this.editTranslationFormGroup.get('text').value;
            const languageId = this.editTranslationFormGroup.get('language').value;
            const comment = this.editTranslationFormGroup.get('comment').value;
            const input = new UpdateAnswerTranslation(text, languageId, comment);
            this._answersService
                .updateAnswerTranslation(answerId, versionId, translationId, input)
                .subscribe(
                    translation => {
                        const translationToReplace = this.currentVersion.translations.find(
                            elem => elem.answerTranslationId === translation.answerTranslationId
                        );
                        const index = this.currentVersion.translations.indexOf(
                            translationToReplace
                        );
                        this.currentVersion.translations[index] = translation;
                        this.editTranslationPressed = false;
                    },
                    error => {
                        console.log(error);
                        this.editTranslationPressed = false;
                    }
                );
        }
    }

    removeTranslation() {
        this.removeTranslationPressed = true;
        if (this.removeTranslationFormGroup.invalid) {
            this.removeTranslationPressed = false;
            return;
        } else {
            const answerId = this.currentAnswer.answerId;
            const versionId = this.currentVersion.answerVersionId;
            const translationId = this.currentTranslation.answerTranslationId;
            const contentCheck: boolean = this.removeTranslationFormGroup.value.ckContentCheck;
            const wantToRemove: boolean = this.removeTranslationFormGroup.value.ckWantToRemove;
            if (contentCheck && wantToRemove) {
                this._answersService
                    .deleteAnswerTranslation(answerId, versionId, translationId)
                    .subscribe(
                        translation => {
                            const translationToRemove = this.currentVersion.translations.find(
                                elem => elem.answerTranslationId === translation.answerTranslationId
                            );
                            const index = this.currentVersion.translations.indexOf(
                                translationToRemove
                            );
                            this.currentVersion.translations[index] = translation;
                            this.removeTranslationFormGroup.reset();
                            this.removeTranslationPressed = false;
                        },
                        error => {
                            console.log(error);
                            this.removeTranslationPressed = false;
                        }
                    );
            } else {
                this.removeTranslationPressed = false;
            }
        }
    }

    assignAnswer(answer: AnswerDTO) {
        this.currentAnswer = answer;
    }

    assignVersion(answer: AnswerDTO, version: AnswerVersionDTO) {
        this.currentAnswer = answer;
        this.currentVersion = version;
    }

    assignTranslation(
        answer: AnswerDTO,
        version: AnswerVersionDTO,
        translation: AnswerTranslationDTO
    ) {
        this.currentAnswer = answer;
        this.currentVersion = version;
        this.currentTranslation = translation;
    }

    getAccordionTitle(answer: AnswerDTO): string {
        let result = 'No english found...';
        for (const version of answer.versions) {
            for (const translation of version.translations) {
                if (translation.isDefaultLanguage && !translation.isDeleted) {
                    if (translation.text.length >= 70) {
                        return translation.text.slice(0, 70) + '[...]';
                    } else {
                        return translation.text;
                    }
                }
            }
        }
        return result;
    }

    get aafg() {
        return this.addAnswerFormGroup.controls;
    }

    tabEvent(index: number, answer: AnswerDTO) {
        answer.activeTab = index;
    }
}
