import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { QuestionnaireDTO } from '../../../../../dto/questionnaires/output/questionnaire-dto';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { SectionsService } from '../../../../../_services/sections.service';
import { SettingsService } from '../../../../../_services/settings.service';
import { TitleDTO } from '../../../../../dto/questionnaires/output/title-dto';
import { SectionDTO } from '../../../../../dto/questionnaires/output/section-dto';
import { QuestionnaireGroupDTO } from '../../../../../dto/questionnaires/output/questionnaire-group-dto';
import { LanguageDTO } from '../../../../../dto/settings/output/language-dto';
import { NgbProgressbarConfig } from '@ng-bootstrap/ng-bootstrap';
import { QuestionsService } from '../../../../../_services/questions.service';
import { AnswersService } from '../../../../../_services/answers.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CreateQuestionTranslation } from '../../../../../dto/questions/input/create-question-translation';
import { CreateAnswerTranslation } from '../../../../../dto/answers/input/create-answer-translation';
import { CreateSection } from '../../../../../dto/questionnaires/input/create-section';
import { OptionDisplayType } from '../../../../../models/enums/display-type.enum';
import { CreateQAMap } from '../../../../../dto/questionnaires/input/create-qamap';
import { Observable } from 'rxjs';
import { delay } from 'rxjs/operators'
import { CreateQuestionnaire } from '../../../../../dto/questionnaires/input/create-questionnaire';
import { concat } from 'rxjs/observable/concat';

@Component({
    selector: 'app-questionnaire-translate',
    templateUrl: './translate-questionnaire.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./translate-questionnaire.component.css']
})
export class TranslateQuestionnaireComponent implements OnInit {
    addQuestionTranslationFormGroup: FormGroup;
    addAnswerTranslationFormGroup: FormGroup;
    languages: LanguageDTO[];
    selectedLanguage: number;
    language: LanguageDTO;
    questionnaireDTO: QuestionnaireDTO;
    questionnaireTranslationGroup: QuestionnaireGroupDTO;
    questionnaireId: number;
    currentStep: number;
    steps: any[];
    progressBarValue: number;
    progressBarStepSize: number;
    currentIndex: number;

    constructor(
        private pbConfig: NgbProgressbarConfig,
        private route: ActivatedRoute,
        private router: Router,
        private qstnService: QuestionsService,
        private answService: AnswersService,
        private qtnrService: QuestionnairesService,
        private sctnService: SectionsService,
        private stngService: SettingsService,
        private builder: FormBuilder
    ) {}

    ngOnInit() {
        // Init

        this.addQuestionTranslationFormGroup = this.builder.group({
            text: ['', Validators.required],
            comment: ['']
        });

        this.addAnswerTranslationFormGroup = this.builder.group({
            text: ['', Validators.required],
            comment: ['']
        });

        // This objets' table will contain all the step for our custom stepper
        this.steps = [];

        this.currentStep = 0;
        this.pbConfig.height = '24px';
        this.pbConfig.max = 100;
        this.pbConfig.animated = true;
        this.progressBarValue = 0;
        // Retrieving the non-deleted non-default languages
        this.stngService.findLanguages().subscribe(languages => {
            this.languages = languages
                .filter(language => !language.isDeleted)
                .filter(language => !language.isDefault);
            this.selectedLanguage = this.languages[0].languageId;
            // The first step is always picking the translation language
            const languageKey = 'translation-language';
            // We push an object with mutliples key-values for the language
            this.steps.push({
                languageKey: this.languages,
                validated: false,
                type: 'LANGUAGE',
                model: this.selectedLanguage
            });
        });
        // Retrieving the questionnaireId from the URL
        this.questionnaireId = this.route.snapshot.params.questionnaireId;

        this.qtnrService.findQuestionnaireById(this.questionnaireId).subscribe(qtnr => {
            this.questionnaireDTO = qtnr;
            // Now that we have the language and the questionnaireDTO, we can use it to build the steps
            this.getSteps(qtnr);
            this.qtnrService
                .findQuestionnaireGroupsById(qtnr.questionnaireTranslationGroupId)
                .subscribe(qtg => {
                    this.questionnaireTranslationGroup = qtg;
                });
        });
    }

    private getSteps(qtnr: QuestionnaireDTO) {
        // Key for the questionnaire title
        const questionnaireTitle = 'questionnaire-title';
        // In each object, we put a validated key set to false, so we can know if the step was validated or not
        // Note that we also put the actual questionnaireTitle in english so we can show the user what he's translating
        this.steps.push({
            questionnaireTitle: qtnr.title,
            validated: false,
            type: 'QTNR-TITLE',
            model: ''
        });

        // Going through each page (section with displayType.PAGE)
        for (let i = 0; i < qtnr.sections.length; i++) {
            // Key for the page title
            const pageTitle = `page-${i}-title`;
            // Same idea as the questionnaire title
            this.steps.push({
                pageTitle: qtnr.sections[i].sectionTitle,
                validated: false,
                type: 'SCTN-TITLE',
                model: ''
            });
            if (qtnr.sections[i].children && qtnr.sections[i].children.length > 0) {
                // Going through each question-answers of this page (children)
                for (let j = 0; j < qtnr.sections[i].children.length; j++) {
                    const qaSection = `qa-${i}-section`;
                    // Retrieving the question translations if any
                    let questionsTranslations = [];
                    /*
                    * Each page-section contain multiple qa-section (question-answer)
                    * We retrieve the ids from the mappingTag
                    * It's done with a regex for the question first and then the answers if any
                    * (TODO : change the translations DTO, so that we can directly access the ids)
                    * Those ids will be used to fetch translations in the chosen language (if any)
                    * The fetchNotDone key allows us to quickly check if the translations were fetched
                    */
                    const section = qtnr.sections[i].children[j];
                    const tag: string = section.map.questionTranslation.mappingTag;
                    let qids = tag.match(/\d+/g).map(Number);
                    let aids = [];

                    if (section.map.answerTranslations) {
                        section.map.answerTranslations.forEach(at => {
                            const currentIds = at.mappingTag.match(/\d+/g).map(Number);
                            aids.push(currentIds);
                        });
                    }

                    this.steps.push({
                        qaSection: qtnr.sections[i].children[j],
                        validated: false,
                        type: 'QA',
                        qIds: qids,
                        aIds: aids,
                        qts: { model: null, qts: [] },
                        atss: [],
                        fetchNotDone: true
                    });
                }
            }
        }
        if (this.steps.length > 0) {
            this.progressBarStepSize = 100 / this.steps.length;
        }
    }

    previous() {
        if (this.currentStep === 0) {
            this.currentStep = this.steps.length - 1;
        } else {
            this.currentStep--;
        }
        this.loadIfQA();
    }

    next() {
        if (this.currentStep === this.steps.length - 1) {
            this.currentStep = 0;
        } else {
            this.currentStep++;
        }
        this.loadIfQA();
    }

    validateStep() {
        if (!this.steps[this.currentStep].validated) {
            switch (this.steps[this.currentStep].type) {
                case 'LANGUAGE':
                    this.language = this.languages.find(
                        l => l.languageId === this.steps[this.currentStep].model
                    );
                    this.steps[this.currentStep].validated = true;
                    this.progressBarValue += this.progressBarStepSize;
                    this.progressBarValue = Math.ceil(this.progressBarValue);
                    this.next();
                    break;
                case 'QTNR-TITLE':
                    let qtnrTitleStep = this.steps[this.currentStep];
                    let qtnrTitleOk = false;
                    if (qtnrTitleStep.model !== null && qtnrTitleStep.model !== '') {
                        qtnrTitleOk = true;
                    }
                    if (qtnrTitleOk) {
                        this.steps[this.currentStep].validated = true;
                        this.progressBarValue += this.progressBarStepSize;
                        this.progressBarValue = Math.ceil(this.progressBarValue);
                        this.next();
                    }
                    break;
                case 'SCTN-TITLE':
                    let sctnTitleStep = this.steps[this.currentStep];
                    let sctnTitleOk = false;
                    if (sctnTitleStep.model !== null && sctnTitleStep.model !== '') {
                        sctnTitleOk = true;
                    }
                    if (sctnTitleOk) {
                        this.steps[this.currentStep].validated = true;
                        this.progressBarValue += this.progressBarStepSize;
                        this.progressBarValue = Math.ceil(this.progressBarValue);
                        this.next();
                    }
                    break;
                case 'QA':
                    let QAStep = this.steps[this.currentStep];
                    let questionOk = false;
                    if (QAStep.qts.model !== null) {
                        questionOk = true;
                    }
                    let answersOk = true;
                    if (QAStep.atss && QAStep.atss.length > 0) {
                        QAStep.atss.forEach(ats => {
                            if (ats.model === null) {
                                answersOk = false;
                            }
                        });
                    }
                    if (questionOk && answersOk) {
                        this.steps[this.currentStep].validated = true;
                        this.progressBarValue += this.progressBarStepSize;
                        this.progressBarValue = Math.ceil(this.progressBarValue);
                        this.next();
                    }
                    break;
                default:
                    this.steps[this.currentStep].validated = true;
                    this.progressBarValue += this.progressBarStepSize;
                    this.progressBarValue = Math.ceil(this.progressBarValue);
                    this.next();
                    break;
            }
        }
    }

    // If we reached a QA step and the translations fetching was not done
    // We fetch the translations and add them to our current step object
    // TODO : use an actual typescript Class so it's cleaner.
    loadIfQA() {
        if (
            this.steps[this.currentStep].type === 'QA' &&
            this.steps[this.currentStep].fetchNotDone
        ) {
            const questionVersionId = this.steps[this.currentStep].qIds[1];
            // Fetch translations if any
            this.qstnService
                .findQuestionTanslationByFilters(
                    [questionVersionId],
                    null,
                    null,
                    [this.selectedLanguage],
                    null
                )
                .subscribe(qts => {
                    if (qts.length > 0) {
                        this.steps[this.currentStep].qts.qts = qts;
                        this.steps[this.currentStep].qts.model = qts[0].questionTranslationId;
                    }
                });

            if (this.steps[this.currentStep].aIds) {
                for (let i = 0; i < this.steps[this.currentStep].aIds.length; i++) {
                    const ids = this.steps[this.currentStep].aIds[i];
                    const answerVersionId = ids[1];
                    this.answService
                        .findAnswerTanslationByFilters(
                            [answerVersionId],
                            null,
                            [this.selectedLanguage],
                            null
                        )
                        .subscribe(ats => {
                            if (ats.length > 0) {
                                this.steps[this.currentStep].atss.push({
                                    index: i,
                                    ats: ats,
                                    model: ats[0].answerTranslationId
                                });
                            } else {
                                this.steps[this.currentStep].atss.push({
                                    index: i,
                                    ats: ats,
                                    model: null
                                });
                            }
                            this.steps[this.currentStep].atss.sort(this.compare);
                        });
                }
            }
            this.steps[this.currentStep].fetchNotDone = false;
        }
    }

    addQuestionTranslation() {
        if (this.addQuestionTranslationFormGroup.invalid) {
            return;
        } else {
            const text: string = this.addQuestionTranslationFormGroup.get('text').value;
            const languageId: number = this.selectedLanguage;
            const comment = this.addQuestionTranslationFormGroup.get('comment').value;

            const questionId = this.steps[this.currentStep].qIds[0];
            const versionId = this.steps[this.currentStep].qIds[1];

            const input = new CreateQuestionTranslation(versionId, text, languageId, comment);
            this.qstnService
                .createQuestionTranslation(questionId, versionId, input)
                .subscribe(qt => {
                    this.steps[this.currentStep].qts.model = qt.questionTranslationId;
                    this.steps[this.currentStep].qts.qts = [
                        ...this.steps[this.currentStep].qts.qts,
                        qt
                    ];
                });

            this.addQuestionTranslationFormGroup.reset();
        }
    }

    addAnswerTranslation() {
        if (this.addAnswerTranslationFormGroup.invalid) {
            return;
        } else {
            const text: string = this.addAnswerTranslationFormGroup.get('text').value;
            const languageId: number = this.selectedLanguage;
            const comment = this.addAnswerTranslationFormGroup.get('comment').value;

            const answerId = this.steps[this.currentStep].aIds[this.currentIndex][0];
            const versionId = this.steps[this.currentStep].aIds[this.currentIndex][1];

            const input = new CreateAnswerTranslation(versionId, text, languageId, comment);
            this.answService.createAnswerTranslation(answerId, versionId, input).subscribe(at => {
                this.steps[this.currentStep].atss[this.currentIndex].model = at.answerTranslationId;
                this.steps[this.currentStep].atss[this.currentIndex].ats = [
                    ...this.steps[this.currentStep].atss[this.currentIndex].ats,
                    at
                ];
            });
            this.addAnswerTranslationFormGroup.reset();
        }
    }

    submit() {
        let sections: CreateSection[] = [];
        let questionnaireTitle: string = '';
        let currentSection: CreateSection = null;

        this.steps.forEach(step => {
            switch (step.type) {
                case 'LANGUAGE':
                    break;
                case 'QTNR-TITLE':
                    questionnaireTitle = step.model;
                    break;
                case 'SCTN-TITLE':
                    currentSection = new CreateSection(step.model, OptionDisplayType.PAGE);
                    sections.push(currentSection);
                    break;
                case 'QA':
                    currentSection.children = currentSection.children
                        ? currentSection.children
                        : new Array<CreateSection>();
                    let child = new CreateSection('', step.qaSection.displayType);
                    if (!child.maps) {
                        child.maps = new Array<CreateQAMap>();
                    }
                    const qtid: number = step.qts.model;
                    if (step.atss && step.atss.length > 0) {
                        for (let i = 0; i < step.atss.length; i++) {
                            const atid: number = step.atss[i].model;
                            child.maps.push(new CreateQAMap(qtid, atid));
                        }
                    } else {
                        child.maps.push(new CreateQAMap(qtid));
                    }
                    currentSection.children.push(child);
                default:
                    break;
            }
        });
        let observables: Observable<SectionDTO>[] = [];

        sections.forEach(page => {
            observables.push(this.sctnService.createSection(page));
        });

        const ids: number[] = [];

        // We first create each page before creating the questionnaire
        Observable.concat(...observables).toArray().subscribe(sctns => {
            sctns.forEach(section => {
                ids.push(section.sectionId);
                console.log(section);
            });
            const createQuestionnaire = new CreateQuestionnaire(
                questionnaireTitle,
                ids,
                this.selectedLanguage,
                this.questionnaireTranslationGroup.questionnaireGroupId
            );
            this.qtnrService.createQuestionnaire(createQuestionnaire).subscribe(qdto => {
                this.router.navigate(['questionnaires/' + qdto.questionnaireId + '/view']);
            });
        });
    }

    assignIndex(index: number) {
        this.currentIndex = index;
    }

    compare(a, b) {
        if (a.index < b.index) return -1;
        if (a.index > b.index) return 1;
        return 0;
    }
}
