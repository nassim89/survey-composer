import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { TranslateQuestionnaireComponent } from './translate-questionnaire.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { SectionsService } from '../../../../../_services/sections.service';
import { SettingsService } from '../../../../../_services/settings.service';
import { MessageService } from '../../../../../_services/message.service';
import { NgbProgressbarConfig, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { ReadOnlyQuestionDisplayModule } from '../../read-only-question-display/read-only-question-display.module';
import { QuestionsService } from '../../../../../_services/questions.service';
import { AnswersService } from '../../../../../_services/answers.service';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: TranslateQuestionnaireComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        ReactiveFormsModule,
        FormsModule,
        NgSelectModule,
        NgbProgressbarModule,
        ReadOnlyQuestionDisplayModule
    ],
    exports: [RouterModule],
    declarations: [TranslateQuestionnaireComponent],
    providers: [QuestionsService, AnswersService, QuestionnairesService, SectionsService, SettingsService, MessageService, NgbProgressbarConfig]
})
export class TranslateQuestionnaireModule { }
