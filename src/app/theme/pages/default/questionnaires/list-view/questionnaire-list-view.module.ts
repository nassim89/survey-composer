import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { QuestionnaireListViewComponent } from './questionnaire-list-view.component';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { MessageService } from '../../../../../_services/message.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { SettingsService } from '../../../../../_services/settings.service';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: QuestionnaireListViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    exports: [RouterModule],
    declarations: [QuestionnaireListViewComponent],
    providers: [QuestionnairesService, SettingsService, MessageService]
})
export class QuestionnaireListViewModule { }
