import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { QuestionnaireDTO } from '../../../../../dto/questionnaires/output/questionnaire-dto';
import { QuestionnaireGroupDTO } from '../../../../../dto/questionnaires/output/questionnaire-group-dto';
import { LanguageDTO } from '../../../../../dto/settings/output/language-dto';
import { SettingsService } from '../../../../../_services/settings.service';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-questionnaire-list-view',
    templateUrl: './questionnaire-list-view.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./questionnaire-list-view.component.css']
})
export class QuestionnaireListViewComponent implements OnInit {
    groupDisplay: any[] = [];
    languages: LanguageDTO[];

    constructor(private qtnrServ: QuestionnairesService, private stngService: SettingsService) { }

    ngOnInit() {
        this.stngService.findLanguages().subscribe(languages => {
            this.languages = languages.filter(l => !l.isDeleted);
        });

        this.qtnrServ.findQuestionnaireGroups().subscribe(groups => {
            groups.forEach(g => {
                this.qtnrServ
                    .findQuestionnairesByQuestionnaireTranslationGroupId(g.questionnaireGroupId)
                    .subscribe(qtnrs => {
                        qtnrs = qtnrs.filter(q => !q.isDeleted);
                        const obj = { group: g, questionnaires: qtnrs };
                        this.groupDisplay.push(obj);
                    });
            });
        });
    }

    deleteQuestionnaire(id: number) {
        this.qtnrServ.deleteQuestionnaire(id).subscribe(qtdo => {
            this.groupDisplay = [];
            this.qtnrServ.findQuestionnaireGroups().subscribe(groups => {
                groups.forEach(g => {
                    this.qtnrServ
                        .findQuestionnairesByQuestionnaireTranslationGroupId(g.questionnaireGroupId)
                        .subscribe(qtnrs => {
                            qtnrs = qtnrs.filter(q => !q.isDeleted);
                            const obj = { group: g, questionnaires: qtnrs };
                            this.groupDisplay.push(obj);
                        });
                });
            });
        });
    }

    downloadQuestionnaire(id: number) {
        this.qtnrServ.downloadQuestionnaire(id).subscribe(data => {
            saveAs(data, `questionnaire_${id}.xls`, { type: 'application/vnd.ms-excel' });
        });
    }
}
