import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { ViewQuestionnaireComponent } from './view-questionnaire.component';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { SettingsService } from '../../../../../_services/settings.service';
import { MessageService } from '../../../../../_services/message.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReadOnlyPageModule } from '../../read-only-page/read-only-page.module';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: ViewQuestionnaireComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        ReadOnlyPageModule
    ],
    exports: [RouterModule],
    declarations: [ViewQuestionnaireComponent],
    providers: [QuestionnairesService, SettingsService, MessageService]
})
export class ViewQuestionnaireModule { }
