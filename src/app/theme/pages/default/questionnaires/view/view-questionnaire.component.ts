import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { QuestionnaireDTO } from '../../../../../dto/questionnaires/output/questionnaire-dto';
import { ActivatedRoute } from '@angular/router';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { QuestionnaireGroupDTO } from '../../../../../dto/questionnaires/output/questionnaire-group-dto';
import { SectionDTO } from '../../../../../dto/questionnaires/output/section-dto';

@Component({
    selector: 'app-questionnaire-view',
    templateUrl: './view-questionnaire.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./view-questionnaire.component.css']
})
export class ViewQuestionnaireComponent implements OnInit {
    _questionnaire: QuestionnaireDTO;
    qtgDTO: QuestionnaireGroupDTO;
    questionnaireId: number;
    selectedPage: SectionDTO;

    constructor(private route: ActivatedRoute, private qtnrServ: QuestionnairesService) { }

    ngOnInit() {
        // Get the questionnaireID
        this.questionnaireId = this.route.snapshot.params.questionnaireId;

        this.qtnrServ.findQuestionnaireById(this.questionnaireId).subscribe(
            q => {
                this._questionnaire = q;
                this.selectedPage = q.sections[0];
                this.qtnrServ
                    .findQuestionnaireGroupsById(q.questionnaireTranslationGroupId)
                    .subscribe(qtg => {
                        this.qtgDTO = qtg;
                    });
            },
            error => {
                console.log(error);
            }
        );
    }

    @Input()
    set questionnaire(questionnaire: QuestionnaireDTO) {
        this._questionnaire = questionnaire;
    }

    get questionnaire(): QuestionnaireDTO {
        return this._questionnaire;
    }
}
