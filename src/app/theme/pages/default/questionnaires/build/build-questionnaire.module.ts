import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../../layouts/layout.module';
import { DefaultComponent } from '../../default.component';
import { BuildQuestionnaireComponent } from './build-questionnaire.component';
import { SectionsService } from '../../../../../_services/sections.service';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { MessageService } from '../../../../../_services/message.service';
import { FormsModule, ReactiveFormsModule } from '../../../../../../../node_modules/@angular/forms';
import { NgSelectModule } from '../../../../../../../node_modules/@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageModule } from '../../page/page.module';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': BuildQuestionnaireComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule,
        ReactiveFormsModule, NgSelectModule, NgbModule, PageModule
    ],
    exports: [
        RouterModule,
    ],
    declarations: [
        BuildQuestionnaireComponent
    ],
    providers: [
        SectionsService, QuestionnairesService, MessageService
    ]
})
export class BuildQuestionnaireModule {
}