import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { SectionsService } from '../../../../../_services/sections.service';
import { QuestionnairesService } from '../../../../../_services/questionnaires.service';
import { CreateQuestionnaireGroup } from '../../../../../dto/questionnaires/input/create-questionnaire-group';
import { CreateSection } from '../../../../../dto/questionnaires/input/create-section';
import { CreateQAMap } from '../../../../../dto/questionnaires/input/create-qamap';
import { CreateQuestionnaire } from '../../../../../dto/questionnaires/input/create-questionnaire';
import { SectionDTO } from '../../../../../dto/questionnaires/output/section-dto';
import { QuestionnaireGroupDTO } from '../../../../../dto/questionnaires/output/questionnaire-group-dto';
import { QuestionnaireDTO } from '../../../../../dto/questionnaires/output/questionnaire-dto';
import { OptionDisplayType } from '../../../../../models/enums/display-type.enum';
import { QA } from '../../../../../models/local/qa';

@Component({
    selector: 'app-questionnaire-build',
    templateUrl: './build-questionnaire.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./build-questionnaire.component.css']
})
export class BuildQuestionnaireComponent implements OnInit {
    questionnaireId: number;
    qdto: QuestionnaireDTO;
    questionnaireTitle: string;
    sectionIds: number[];
    questionnaireTranslationGroupId: number;
    languageId: number;
    questionnaireFormGroup: FormGroup;
    addTranslationGroupFormGroup: FormGroup;
    questionnaireTranslationGroupsDTO: QuestionnaireGroupDTO[];
    pages: CreateSection[];
    selectedPage: CreateSection;
    questionnaireDTO: QuestionnaireDTO;
    selectedGroup: number;
    addGroupPressed = false;
    addPagePressed = false;
    removePagePressed = false;
    questionnaireSubmitPressed = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private sctnServ: SectionsService,
        private qtnrServ: QuestionnairesService,
        private builder: FormBuilder,
        private router: Router
    ) {}

    ngOnInit() {
        if (
            this.activatedRoute.snapshot.params &&
            this.activatedRoute.snapshot.params.questionnaireId
        ) {
            this.questionnaireId = this.activatedRoute.snapshot.params.questionnaireId;

            this.qtnrServ.findQuestionnaireById(this.questionnaireId).subscribe(
                qtnr => {
                    this.qdto = qtnr;
                    this.prefil(this.qdto);
                },
                error => {
                    this.router.navigate(['questionnaires/build']);
                }
            );
        }
        this.questionnaireFormGroup = this.builder.group({
            questionnaireTitle: [this.questionnaireTitle, Validators.required],
            groupId: []
        });

        this.addTranslationGroupFormGroup = this.builder.group({
            groupName: ['', Validators.required],
            description: ['']
        });

        this.qtnrServ.findQuestionnaireGroups().subscribe(
            groups => {
                this.questionnaireTranslationGroupsDTO = groups;
            },
            error => {
                console.log(error);
            }
        );

        this.pages = new Array<CreateSection>();
    }

    addTranslationGroup() {
        this.addGroupPressed = true;
        if (this.addTranslationGroupFormGroup.invalid) {
            this.addGroupPressed = false;
            return;
        } else {
            let groupName: string = this.addTranslationGroupFormGroup.value.groupName;
            groupName = groupName.trim();
            let description: string = this.addTranslationGroupFormGroup.value.description;
            description = description.trim();

            const input = new CreateQuestionnaireGroup(groupName, description);
            this.qtnrServ.createQuestionnaireGroup(input).subscribe(
                group => {
                    this.questionnaireTranslationGroupsDTO = [
                        ...this.questionnaireTranslationGroupsDTO,
                        group
                    ];
                    this.addGroupPressed = false;
                },
                error => {
                    console.log(error);
                    this.addGroupPressed = false;
                }
            );
        }
    }

    addPage() {
        this.addPagePressed = true;
        const page = new CreateSection(
            `Page Title`,
            OptionDisplayType.PAGE,
            null,
            null,
            this.pages.length + 1
        );
        this.selectedPage = page;
        this.pages = [...this.pages, page];
        this.addPagePressed = false;
    }

    delete(page: CreateSection) {
        this.removePagePressed = true;
        this.pages = this.pages.filter(p => p.sectionNumber !== page.sectionNumber);
        if (this.pages.length > 0) {
            for (let i = 0; i < this.pages.length; i++) {
                this.pages[i].sectionNumber = i + 1;
            }
            this.selectedPage = this.pages[this.pages.length - 1];
        } else {
            this.selectedPage = null;
        }
        this.removePagePressed = false;
    }

    questionnaireSubmit() {
        this.questionnaireSubmitPressed = true;
        if (this.questionnaireFormGroup.invalid) {
            this.questionnaireSubmitPressed = false;
            return;
        } else {
            // Submit each page, gather the IDs
            const ids: number[] = [];
            // Collecting all observables in an array and using forkJoin to wait for all of them to complete
            let observables: Observable<SectionDTO>[] = [];
            this.pages.forEach(page => {
                // Get the QAMaps from our QA object
                page.children.forEach(child => {
                    if (
                        child.displayType === 0 ||
                        child.displayType === 4 ||
                        child.displayType === 5
                    ) {
                        child.qa.answers = null;
                    }
                    child.maps = this.createQAMap(child.qa);
                });
                observables.push(this.sctnServ.createSection(page));
            });

            Observable.forkJoin(observables).subscribe(sctns => {
                sctns.forEach(section => {
                    ids.push(section.sectionId);
                });
                // Submitting the questionnaire
                const title: string = this.questionnaireFormGroup.value.questionnaireTitle;
                let groupId = null;
                if (this.questionnaireFormGroup.value.groupId) {
                    groupId = this.questionnaireFormGroup.value.groupId;
                }
                const createQuestionnaire = new CreateQuestionnaire(title, ids, 1, groupId);

                this.qtnrServ.createQuestionnaire(createQuestionnaire).subscribe(
                    qdto => {
                        this.questionnaireDTO = qdto;
                        this.router.navigate([
                            'questionnaires/' + this.questionnaireDTO.questionnaireId + '/view'
                        ]);
                        this.questionnaireSubmitPressed = false;
                    },
                    error => {
                        this.questionnaireSubmitPressed = false;
                        console.log(error);
                    }
                );
            }, error => {
                console.log(error);
                this.questionnaireSubmitPressed = false;
            });
        }
    }

    createQAMap(qa: QA): CreateQAMap[] {
        const arrayOfQAMap: CreateQAMap[] = new Array<CreateQAMap>();
        if (qa && qa.question && qa.answers && qa.answers.length > 0) {
            const qtid = qa.question.questionTranslationId;
            qa.answers.forEach(answer => {
                const atid = answer.answerTranslationId;
                arrayOfQAMap.push(new CreateQAMap(qtid, atid));
            });
            return arrayOfQAMap;
        } else if (qa && qa.question) {
            const qtid = qa.question.questionTranslationId;
            arrayOfQAMap.push(new CreateQAMap(qtid));
            return arrayOfQAMap;
        } else {
            return arrayOfQAMap;
        }
    }

    prefil(qdto: QuestionnaireDTO) {
        this.questionnaireTitle = qdto.title.defaultTitleText;
        this.selectedGroup = qdto.questionnaireTranslationGroupId;
        const pageFilled: CreateSection[] = new Array<CreateSection>();

        for (let i = 0; i < qdto.sections.length; i++) {
            const page = qdto.sections[i];
            const children: CreateSection[] = new Array<CreateSection>();

            if (page.children) {
                for (let j = 0; j < page.children.length; j++) {
                    const child = page.children[j];
                    const cs = new CreateSection(
                        child.sectionTitle.defaultTitleText,
                        child.displayType,
                        null,
                        null,
                        j + 1,
                        null,
                        null
                    );
                    cs.qa = new QA(child.map.questionTranslation);
                    if (child.map.answerTranslations && child.map.answerTranslations.length > 0) {
                        child.map.answerTranslations.forEach(at => {
                            cs.qa.addAnswer(at);
                        });
                    }
                    children.push(cs);
                }
            } else {
                return;
            }

            const p = new CreateSection(
                page.sectionTitle.defaultTitleText,
                page.displayType,
                null,
                null,
                i + 1,
                null,
                children
            );

            pageFilled.push(p);
        }
        this.pages = pageFilled;
        if (this.pages && this.pages.length > 0) {
            this.selectedPage = this.pages[0];
        }
    }

    // return true if each page has a child section (qa)
    canSubmit(): boolean {
        let canSubmit = true;
        if (this.pages && this.pages.length >= 1) {
            this.pages.forEach(page => {
                canSubmit = canSubmit && (page.children && page.children.length >= 1);
            })
        } else {
            canSubmit = false;
        }
        return canSubmit;
    }
}
