import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AnswersService } from '../../../../_services/answers.service';
import { AnswerCategoryDTO } from '../../../../dto/answers/output/answer-category-dto';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { CreateCategory } from '../../../../dto/questions/input/create-category';
import { UpdateCategory } from '../../../../dto/questions/input/update-category';
import { AnswerCategory } from '../../../../models/answers/answer-category';

@Component({
    selector: 'app-categories-answers',
    templateUrl: './categories-answers.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./categories-answers.component.css']
})
export class CategoriesAnswersComponent implements OnInit, AfterViewInit {
    categories: AnswerCategoryDTO[];
    categoriesWithNested: AnswerCategoryDTO[];
    gridApi;
    gridColumnApi;
    rowData: any[];
    columnDefs: any[];
    rowClassRules: any;
    selectedCategory: AnswerCategoryDTO;
    multiSortKey: string;
    paginationPageSize = 11;
    addCategoryForm: FormGroup;
    updateCategoryForm: FormGroup;
    deleteCategoryForm: FormGroup;
    cedPressed = false;

    constructor(
        private _scriptLoader: ScriptLoaderService,
        private _answersService: AnswersService,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit() {
        this.addCategoryForm = this.formBuilder.group({
            categoryName: ['', Validators.required],
            description: [''],
            parentCategory: []
        });

        this.updateCategoryForm = this.formBuilder.group({
            categorySelect: [],
            categoryName: ['', Validators.required],
            description: ['']
        });

        this.deleteCategoryForm = this.formBuilder.group({
            categorySelect: [, Validators.required],
            ckContentCheck: [false, Validators.required],
            ckWantToRemove: [false, Validators.required]
        });

        this.columnDefs = this.getColumnDefs();
        this.rowClassRules = this.getRowClassRules();
        this.multiSortKey = 'ctrl';
    }

    ngAfterViewInit() {}

    // TODO to be eventually defined directly from the categories
    getColumnDefs(): any[] {
        return [
            {
                headerName: 'Category Name',
                field: 'categoryName',
                cellStyle: { 'font-weight': 'bold' },
                filter: 'agTextColumnFilter'
            },
            { headerName: 'Description', field: 'description' },
            { headerName: 'Parent Name', field: 'parentName' },
            { headerName: 'Type', field: 'categoryType' },
            { headerName: 'Last modification', field: 'lastModificationDate', suppressFilter: true }
        ];
    }

    getRowClassRules(): any {
        return {
            'is-parent': function(params) {
                return params.data.categoryType === 'MAIN CATEGORY';
            },
            'is-child': function(params) {
                return params.data.categoryType === 'SUB CATEGORY';
            }
        };
    }

    getCategories(categories): any[] {
        const result = [];
        if (categories) {
            for (let category of categories) {
                if (category.children && category.children.length > 0) {
                    category.categoryType = 'MAIN CATEGORY';
                    result.push(category);
                    for (let child of category.children) {
                        child.categoryType = 'SUB CATEGORY';
                        child.parentName = category.categoryName;
                        result.push(child);
                    }
                } else {
                    category.categoryType = 'MAIN CATEGORY';
                    category.parentName = '';
                    result.push(category);
                }
            }
        }
        return result;
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this._answersService.findAnswerCategories(false).subscribe(categories => {
            this.categories = categories.filter(c => c.isDeleted === false);
            this.categoriesWithNested = this.getFlatCategories(categories).filter(
                c => c.isDeleted === false
            );
            this.rowData = this.getCategories(categories).filter(c => c.isDeleted === false);
            params.api.sizeColumnsToFit();
        });
    }

    getFlatCategories(categories: AnswerCategoryDTO[]): AnswerCategoryDTO[] {
        const result = new Array<AnswerCategoryDTO>();
        for (const cat of categories) {
            result.push(cat);
            if (cat.children && cat.children.length > 0) {
                for (const child of cat.children) {
                    result.push(child);
                }
            }
        }
        return result;
    }

    fetchCategoriesAndRefresh() {
        this._answersService.findAnswerCategories(false).subscribe(categories => {
            this.categories = categories.filter(c => c.isDeleted === false);
            this.categoriesWithNested = this.getFlatCategories(categories).filter(
                c => c.isDeleted === false
            );
            this.rowData = this.getCategories(categories).filter(c => c.isDeleted === false);
            this.gridApi.refreshCells();
            this.gridApi.sizeColumnsToFit();
        });
    }

    onRowClicked(event: any) {
        if (event && event.data) {
            this.selectedCategory = event.data;
        }
    }

    onCreate() {
        this.cedPressed = true;
        if (this.addCategoryForm.invalid) {
            this.cedPressed = false;
            return;
        } else {
            let categoryName: string = this.addCategoryForm.value.categoryName;
            categoryName = categoryName.trim();

            let description: string = this.addCategoryForm.value.description;
            description = description.trim();

            const parentCategory = this.addCategoryForm.value.parentCategory;

            const createCat = new CreateCategory(categoryName, description, parentCategory);

            this._answersService.createAnswerCategory(createCat).subscribe(
                cat => {
                    if (cat !== undefined) {
                        this.fetchCategoriesAndRefresh();
                        this.selectedCategory = cat;
                    }
                    this.cedPressed = false;
                },
                error => {
                    console.log(error);
                    this.cedPressed = false;
                }
            );
        }
    }

    selectChange(event) {
        if (event) {
            this.selectedCategory = event;
        }
    }

    onUpdate() {
        this.cedPressed = true;
        if (this.updateCategoryForm.invalid) {
            this.cedPressed = false;
            return;
        } else {
            let categoryId = this.updateCategoryForm.value.categorySelect;

            let categoryName: string = this.updateCategoryForm.value.categoryName;
            categoryName = categoryName.trim();

            let description: string = this.updateCategoryForm.value.description;
            description = description.trim();

            this._answersService
                .updateAnswerCategory(categoryId, new UpdateCategory(categoryName, description))
                .subscribe(
                    cat => {
                        if (cat !== undefined) {
                            this.fetchCategoriesAndRefresh();
                            this.selectedCategory = cat;
                        }
                        this.cedPressed = false;
                    },
                    error => {
                        this.cedPressed = false;
                        console.log(error);
                    }
                );
        }
    }

    onDelete() {
        this.cedPressed = true;
        if (this.deleteCategoryForm.invalid) {
            this.cedPressed = false;
            return;
        } else {
            let categoryId: number = this.deleteCategoryForm.value.categorySelect;
            let contentCheck: boolean = this.deleteCategoryForm.value.ckContentCheck;
            let wantToRemove: boolean = this.deleteCategoryForm.value.ckWantToRemove;
            if (contentCheck && wantToRemove) {
                this._answersService.deleteAnswerCategory(categoryId).subscribe(
                    cat => {
                        if (cat !== undefined) {
                            this.fetchCategoriesAndRefresh();
                            this.deleteCategoryForm.reset();
                        }
                        this.cedPressed = false;
                    },
                    error => {
                        this.cedPressed = false;
                        console.log(error);
                    }
                );
            }
        }
    }

    get cf() {
        return this.addCategoryForm.controls;
    }

    get uf() {
        return this.updateCategoryForm.controls;
    }

    get df() {
        return this.deleteCategoryForm.controls;
    }
}
