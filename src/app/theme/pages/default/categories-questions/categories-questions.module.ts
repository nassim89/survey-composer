import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { CategoriesQuestionsComponent } from './categories-questions.component';
import { QuestionsService } from '../../../../_services/questions.service';
import { MessageService } from '../../../../_services/message.service';
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { FilterDeletedPipe } from '../../../../_pipes/filter-deleted.pipe';
import { SharedPipesModule } from '../../../../_pipes/shared-pipes.module';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': CategoriesQuestionsComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        AgGridModule.withComponents([]),
        NgSelectModule,
        SharedPipesModule
    ], exports: [
        RouterModule,
    ], declarations: [
        CategoriesQuestionsComponent,
    ], providers: [
        QuestionsService, MessageService
    ]
})
export class CategoriesQuestionsModule {
}