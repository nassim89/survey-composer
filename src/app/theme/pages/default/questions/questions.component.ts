import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { QuestionsService } from '../../../../_services/questions.service';
import { QuestionCategoryDTO } from '../../../../dto/questions/output/question-category-dto';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { QuestionDTO } from '../../../../dto/questions/output/question-dto';
import { ActivatedRoute } from '../../../../../../node_modules/@angular/router';
import { CreateQuestion } from '../../../../dto/questions/input/create-question';
import { QuestionVersionDTO } from '../../../../dto/questions/output/question-version-dto';
import { CreateQuestionVersion } from '../../../../dto/questions/input/create-question-version';
import { QuestionTranslationDTO } from '../../../../dto/questions/output/question-translation-dto';
import { CreateQuestionTranslation } from '../../../../dto/questions/input/create-question-translation';
import { LanguageDTO } from '../../../../dto/settings/output/language-dto';
import { UpdateQuestionTranslation } from '../../../../dto/questions/input/update-question-translation';
import { SettingsService } from '../../../../_services/settings.service';
import { ActionType } from '../../../../models/enums/action-type.enum';
import { LabelDTO } from '../../../../dto/questions/output/label-dto';
import { Helpers } from '../../../../helpers';

@Component({
    selector: 'app-questions',
    templateUrl: './questions.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit, AfterViewInit {
    category: QuestionCategoryDTO;
    questions: QuestionDTO[];
    labels: LabelDTO[];
    selectedLabels: LabelDTO[];
    languages: LanguageDTO[];
    allButEnglish: LanguageDTO[];
    onlyEnglish: LanguageDTO[];
    questionCategoryId: number;
    addQuestionFormGroup: FormGroup;
    addVersionFormGroup: FormGroup;
    addTranslationFormGroup: FormGroup;
    removeVersionFormGroup: FormGroup;
    editTranslationFormGroup: FormGroup;
    removeTranslationFormGroup: FormGroup;
    addQuestionSubmitted: boolean = false;
    currentQuestion: QuestionDTO;
    currentVersion: QuestionVersionDTO;
    currentTranslation: QuestionTranslationDTO;
    enumAction = ActionType;
    activeTab: number = 0;
    addQuestionPressed = false;
    addVersionPressed = false;
    removeVersionPressed = false;
    addTranslationPressed = false;
    editTranslationPressed = false;
    removeTranslationPressed = false;

    constructor(
        private route: ActivatedRoute,
        private _questionsService: QuestionsService,
        private _settingsService: SettingsService,
        private formBuilder: FormBuilder
    ) {
        this.addQuestionFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            labels: [],
            description: [''],
            comment: ['']
        });

        this.addVersionFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            comment: ['']
        });

        this.removeVersionFormGroup = this.formBuilder.group({
            ckContentCheck: [false, Validators.required],
            ckWantToRemove: [false, Validators.required]
        });

        this.addTranslationFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            language: [null, Validators.required],
            comment: ['']
        });

        this.editTranslationFormGroup = this.formBuilder.group({
            text: ['', Validators.required],
            language: [null, Validators.required],
            comment: ['']
        });

        this.removeTranslationFormGroup = this.formBuilder.group({
            ckContentCheck: [false, Validators.required],
            ckWantToRemove: [false, Validators.required]
        });
    }

    ngOnInit() {
        // Get the questionCategoryId
        this.questionCategoryId = this.route.snapshot.params.questionCategoryId;
        // Get the category from the questionCategoryId
        this._questionsService.findQuestionCategoriesById(this.questionCategoryId).subscribe(
            cat => {
                this.category = cat;
            },
            error => {
                console.log('Retrieving the category failed.');
                console.log(error);
            }
        );
        // Get the labels
        this._questionsService.findLabels().subscribe(
            labels => {
                this.labels = labels.filter(l => !l.isDeleted);
            },
            error => {
                console.log(error);
            }
        );
        // Get the languages
        this._settingsService.findLanguages().subscribe(
            languages => {
                this.languages = languages;
                this.allButEnglish = this.languages
                    .filter(l => !l.isDefault)
                    .filter(l => !l.isDeleted);
                this.onlyEnglish = this.languages
                    .filter(l => l.isDefault)
                    .filter(l => !l.isDeleted);
            },
            error => {
                console.log('Retrieving the languages failed.');
                console.log(error);
            }
        );

        // Get the questions from the questionCategoryId
        this._questionsService.findQuestions(this.questionCategoryId).subscribe(
            questions => {
                this.questions = questions;
                this.questions.forEach(q => {
                    q.activeTab = 0;
                });
            },
            error => {
                console.log('Retrieving the questions failed.');
                console.log(error);
            }
        );
    }

    ngAfterViewInit() {}

    addQuestion() {
        this.addQuestionPressed = true;
        if (this.addQuestionFormGroup.invalid) {
            this.addQuestionPressed = false;
            return;
        } else {
            let text: string = this.addQuestionFormGroup.get('text').value;
            const description = this.addQuestionFormGroup.get('description').value;
            const comment = this.addQuestionFormGroup.get('comment').value;

            let labels = null;

            if (this.addQuestionFormGroup.get('labels').value) {
                labels = (<LabelDTO[]>this.addQuestionFormGroup.get('labels').value).map(
                    label => label.labelId
                );
            }

            const input = new CreateQuestion(
                text,
                this.questionCategoryId,
                description,
                comment,
                labels
            );
            this._questionsService.createQuestion(input).subscribe(
                question => {
                    question.activeTab = 0;
                    this.questions.unshift(question);
                    this.addQuestionFormGroup.reset();
                    this.addQuestionPressed = false;
                },
                error => {
                    this.addQuestionPressed = false;
                    console.log(error);
                }
            );
        }
    }

    addVersion() {
        this.addVersionPressed = true;
        if (this.addVersionFormGroup.invalid) {
            this.addVersionPressed = false;
            return;
        } else {
            let text: string = this.addVersionFormGroup.get('text').value;
            const comment = this.addVersionFormGroup.get('comment').value;

            const input = new CreateQuestionVersion(this.currentQuestion.questionId, text, comment);

            this._questionsService.createVersion(this.currentQuestion.questionId, input).subscribe(
                version => {
                    this.currentQuestion.versions.unshift(version);
                    this.currentQuestion.activeTab = 0;
                    this.addVersionFormGroup.reset();
                    this.addVersionPressed = false;
                },
                error => {
                    this.addVersionPressed = false;
                    console.log(error);
                }
            );
        }
    }

    removeVersion() {
        this.removeVersionPressed = true;
        if (this.removeVersionFormGroup.invalid) {
            this.removeVersionPressed = false;
            return;
        } else {
            const questionId: number = this.currentQuestion.questionId;
            const versionId: number = this.currentVersion.questionVersionId;
            const contentCheck: boolean = this.removeVersionFormGroup.value.ckContentCheck;
            const wantToRemove: boolean = this.removeVersionFormGroup.value.ckWantToRemove;
            if (contentCheck && wantToRemove) {
                const count = this.currentQuestion.versions.filter(version => !version.isDeleted)
                    .length;
                if (count < 2) {
                    this._questionsService.deleteQuestion(questionId).subscribe(
                        question => {
                            const questionToRemove = this.questions.find(
                                elem => elem.questionId === question.questionId
                            );
                            const index = this.questions.indexOf(questionToRemove);
                            this.questions[index] = question;
                            this.removeVersionFormGroup.reset();
                            this.removeVersionPressed = false;
                        },
                        error => {
                            this.removeVersionPressed = false;
                            console.log('error while deleting this question');
                        }
                    );
                } else {
                    this._questionsService.deleteQuestionVersion(questionId, versionId).subscribe(
                        version => {
                            const versionToRemove = this.currentQuestion.versions.find(
                                elem => elem.questionVersionId === version.questionVersionId
                            );
                            const index = this.currentQuestion.versions.indexOf(versionToRemove);
                            this.currentQuestion.versions[index] = version;
                            this.currentQuestion.activeTab = 0;
                            this.removeVersionFormGroup.reset();
                            this.removeVersionPressed = false;
                        },
                        error => {
                            this.removeVersionPressed = false;
                            console.log(error);
                        }
                    );
                }
            } else {
                this.removeVersionPressed = false;
            }
        }
    }

    addTranslation() {
        this.addTranslationPressed = true;
        if (this.addTranslationFormGroup.invalid) {
            this.addTranslationPressed = false;
            return;
        } else {
            const questionId = this.currentQuestion.questionId;
            const versionId = this.currentVersion.questionVersionId;
            const text: string = this.addTranslationFormGroup.get('text').value;
            const languageId: number = this.addTranslationFormGroup.get('language').value;
            const comment = this.addTranslationFormGroup.get('comment').value;
            const input = new CreateQuestionTranslation(versionId, text, languageId, comment);

            this._questionsService
                .createQuestionTranslation(questionId, versionId, input)
                .subscribe(
                    translation => {
                        this.currentVersion.translations.unshift(translation);
                        this.addTranslationFormGroup.reset();
                        this.addTranslationPressed = false;
                    },
                    error => {
                        this.addTranslationPressed = false;
                        console.log(error);
                    }
                );
        }
    }

    editTranslation() {
        this.editTranslationPressed = true;
        if (this.editTranslationFormGroup.invalid) {
            this.editTranslationPressed = false;
            return;
        } else {
            const questionId = this.currentQuestion.questionId;
            const versionId = this.currentVersion.questionVersionId;
            const translationId = this.currentTranslation.questionTranslationId;
            const text = this.editTranslationFormGroup.get('text').value;
            const languageId = this.editTranslationFormGroup.get('language').value;
            const comment = this.editTranslationFormGroup.get('comment').value;
            const input = new UpdateQuestionTranslation(text, languageId, comment);
            this._questionsService
                .updateQuestionTranslation(questionId, versionId, translationId, input)
                .subscribe(
                    translation => {
                        const translationToReplace = this.currentVersion.translations.find(
                            elem => elem.questionTranslationId === translation.questionTranslationId
                        );
                        const index = this.currentVersion.translations.indexOf(
                            translationToReplace
                        );
                        this.currentVersion.translations[index] = translation;
                        this.editTranslationPressed = false;
                    },
                    error => {
                        this.editTranslationPressed = true;
                        console.log(error);
                    }
                );
        }
    }

    removeTranslation() {
        this.removeTranslationPressed = true;
        if (this.removeTranslationFormGroup.invalid) {
            this.removeTranslationPressed = false;
            return;
        } else {
            console.log(this.currentTranslation);
            const questionId = this.currentQuestion.questionId;
            const versionId = this.currentVersion.questionVersionId;
            const translationId = this.currentTranslation.questionTranslationId;
            const contentCheck: boolean = this.removeTranslationFormGroup.value.ckContentCheck;
            const wantToRemove: boolean = this.removeTranslationFormGroup.value.ckWantToRemove;
            if (contentCheck && wantToRemove) {
                this._questionsService
                    .deleteQuestionTranslation(questionId, versionId, translationId)
                    .subscribe(
                        translation => {
                            const translationToRemove = this.currentVersion.translations.find(
                                elem =>
                                    elem.questionTranslationId === translation.questionTranslationId
                            );
                            const index = this.currentVersion.translations.indexOf(
                                translationToRemove
                            );
                            this.currentVersion.translations[index] = translation;
                            this.removeTranslationFormGroup.reset();
                            this.removeTranslationPressed = false;
                        },
                        error => {
                            this.removeTranslationPressed = false;
                            console.log(error);
                        }
                    );
            } else {
                this.removeTranslationPressed = false;
            }
        }
    }

    assignQuestion(question: QuestionDTO) {
        this.currentQuestion = question;
    }

    assignVersion(question: QuestionDTO, version: QuestionVersionDTO) {
        this.currentQuestion = question;
        this.currentVersion = version;
    }

    assignTranslation(
        question: QuestionDTO,
        version: QuestionVersionDTO,
        translation: QuestionTranslationDTO
    ) {
        this.currentQuestion = question;
        this.currentVersion = version;
        this.currentTranslation = translation;
    }

    getAccordionTitle(question: QuestionDTO): string {
        let result = 'No english found...';
        for (const version of question.versions) {
            for (const translation of version.translations) {
                if (translation.isDefaultLanguage && !translation.isDeleted) {
                    if (translation.text.length >= 70) {
                        return translation.text.slice(0, 70) + '[...]';
                    } else {
                        return translation.text;
                    }
                }
            }
        }
        return result;
    }

    get aafg() {
        return this.addQuestionFormGroup.controls;
    }

    tabEvent(index: number, question: QuestionDTO) {
        question.activeTab = index;
    }
}
