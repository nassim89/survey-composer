import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { QuestionsComponent } from './questions.component';
import { QuestionsService } from '../../../../_services/questions.service';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { NgSelectModule } from '../../../../../../node_modules/@ng-select/ng-select';
import { MessageService } from '../../../../_services/message.service';
import { SharedPipesModule } from '../../../../_pipes/shared-pipes.module';
import { SettingsService } from '../../../../_services/settings.service';

const routes: Routes = [
    {
        'path': '',
        'component': DefaultComponent,
        'children': [
            {
                'path': '',
                'component': QuestionsComponent,
            },
        ],
    },
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        SharedPipesModule
    ], exports: [
        RouterModule,
    ], declarations: [
        QuestionsComponent,
    ], providers: [
        QuestionsService, SettingsService, MessageService
    ]
})
export class QuestionsModule {
}