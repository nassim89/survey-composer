import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { AnswersService } from '../../../../_services/answers.service';
import { AnswerCategoryDTO } from '../../../../dto/answers/output/answer-category-dto';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { CreateCategory } from '../../../../dto/questions/input/create-category';
import { UpdateCategory } from '../../../../dto/questions/input/update-category';
import { AnswerCategory } from '../../../../models/answers/answer-category';
import { LanguageDTO } from '../../../../dto/settings/output/language-dto';
import { UpdateLanguage } from '../../../../dto/settings/input/update-language';
import { SettingsService } from '../../../../_services/settings.service';
import { CreateLanguage } from '../../../../dto/settings/input/create-language';

@Component({
    selector: 'app-languages',
    templateUrl: './languages.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./languages.component.css']
})

export class LanguagesComponent implements OnInit, AfterViewInit {
    languages: LanguageDTO[];
    gridApi;
    gridColumnApi;
    rowData: any[];
    columnDefs: any[];
    selectedLanguage: LanguageDTO;
    multiSortKey: string;
    createSubmitted = false;
    updateSubmitted = false;
    paginationPageSize = 15;
    updateLanguage: UpdateLanguage;
    addLanguageForm: FormGroup;
    updateLanguageForm: FormGroup;
    deleteLanguageForm: FormGroup;

    constructor(private _scriptLoader: ScriptLoaderService,
        private _settingsService: SettingsService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {

        this.selectedLanguage = new LanguageDTO();

        this.addLanguageForm = this.formBuilder.group({
            languageCode: ['', Validators.required],
            description: ['']
        });

        this.updateLanguageForm = this.formBuilder.group({
            languageCode: ['', Validators.required],
            description: [''],
            languageSelect: []
        });

        this.deleteLanguageForm = this.formBuilder.group({
            languageSelect: [],
            ckWantToRemove: [false, Validators.required]
        })

        this.updateLanguage = new UpdateLanguage('', '');
        this.columnDefs = this.getColumnDefs();
        this.multiSortKey = 'ctrl';
    }

    ngAfterViewInit() {
    }

    // TODO to be eventually defined directly from the categories
    getColumnDefs(): any[] {
        return [
            { headerName: 'Language Code', field: 'languageCode', cellStyle: { 'font-weight': 'bold' }, filter: 'agTextColumnFilter' },
            { headerName: 'Language Description', field: 'description' },
            { headerName: 'Is default', field: 'isDefault' }
        ];
    }


    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this._settingsService.findLanguages().subscribe(
            languages => {
                this.languages = languages.filter(l => l.isDeleted === false);
                this.rowData = this.languages;
                params.api.sizeColumnsToFit();
            }
        )

    }

    fetchLanguageAndRefresh() {
        this._settingsService.findLanguages().subscribe(
            languages => {
                this.languages = languages.filter(l => l.isDeleted === false);
                this.rowData = this.languages;
                this.gridApi.refreshCells();
                this.gridApi.sizeColumnsToFit();
            }
        )
    }

    onRowClicked(event: any) {
        if (event && event.data) {
            this.updateLanguage.languageCode = event.data.languageCode;
            this.updateLanguage.description = event.data.description;
            this.selectedLanguage = event.data;
        }
    }

    onCreate() {
        if (this.addLanguageForm.invalid) {
            return;
        } else {
            let languageCode: string = this.addLanguageForm.value.languageCode;

            let description: string = this.addLanguageForm.value.description;

            const createLang = new CreateLanguage(languageCode, description);

            this._settingsService.createLanguage(createLang).subscribe(
                language => {
                    if (language !== undefined) {
                        this.fetchLanguageAndRefresh();
                        this.selectedLanguage = language;
                    }
                },
                error => {
                    console.log('Error while retrieving languages.')
                }
            );
        }
    }

    onSelectToUpdateChange(event) {
        this.selectedLanguage = event;
        if (this.selectedLanguage) {
            this.updateLanguage.languageCode = this.selectedLanguage.languageCode;
            this.updateLanguage.description = this.selectedLanguage.description;
        }
    }

    onUpdate() {
        if (this.updateLanguageForm.invalid) {
            return;
        } else {
            let languageCode: string = this.updateLanguageForm.value.languageCode;

            let description: string = this.updateLanguageForm.value.description;

            const updateLang = new UpdateLanguage(languageCode, description);

            this._settingsService.updateLanguage(this.selectedLanguage.languageId, updateLang).subscribe(
                language => {
                    if (language !== undefined) {
                        this.fetchLanguageAndRefresh();
                        this.selectedLanguage = language;
                    }
                },
                error => {
                    console.log('Error while retrieving languages.');
                    console.log(error);
                }
            );
        }
    }

    onDelete() {
        if (this.deleteLanguageForm.invalid) {
            return;
        } else {
            let wantToRemove: boolean = this.deleteLanguageForm.value.ckWantToRemove;
            if (wantToRemove) {
                this._settingsService.deleteLanguage(this.selectedLanguage.languageId).subscribe(
                    cat => {
                        if (cat !== undefined) {
                            this.fetchLanguageAndRefresh();
                            this.deleteLanguageForm.reset();
                        }
                    },
                    error => {
                        console.log('Error while deleting language.');
                        console.log(error);
                    }
                )
            }
        }
    }
}