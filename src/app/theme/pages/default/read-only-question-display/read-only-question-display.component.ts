import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { AnswerTranslationDTO } from '../../../../dto/answers/output/answer-translation-dto';
import { SectionDTO } from '../../../../dto/questionnaires/output/section-dto';
import { QA } from '../../../../models/local/qa';

@Component({
    selector: 'app-read-only-question-display',
    templateUrl: 'read-only-question-display.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./read-only-question-display.component.css']
})
export class ReadOnlyQuestionDisplayComponent implements OnInit {
    _section: SectionDTO;

    constructor() { }

    ngOnInit() { }

    @Input()
    set section(section: SectionDTO) {
        this._section = section;
    }

    get section(): SectionDTO {
        return this._section;
    }
}
