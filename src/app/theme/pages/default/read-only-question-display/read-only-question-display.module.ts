import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReadOnlyQuestionDisplayComponent } from './read-only-question-display.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, NgSelectModule],
    exports: [ReadOnlyQuestionDisplayComponent],
    declarations: [ReadOnlyQuestionDisplayComponent],
    providers: []
})
export class ReadOnlyQuestionDisplayModule { }
