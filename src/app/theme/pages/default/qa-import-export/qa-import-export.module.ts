import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../default.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { QAImportExportComponent } from './qa-import-export.component';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { QuestionsService } from '../../../../_services/questions.service';
import { AnswersService } from '../../../../_services/answers.service';
import { SettingsService } from '../../../../_services/settings.service';
import { MessageService } from '../../../../_services/message.service';

const routes: Routes = [
    {
        path: '',
        component: DefaultComponent,
        children: [
            {
                path: '',
                component: QAImportExportComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        LayoutModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule
    ],
    exports: [RouterModule, QAImportExportComponent],
    declarations: [QAImportExportComponent],
    providers: [QuestionsService, AnswersService, SettingsService, MessageService]
})
export class QAImportExportModule {}
