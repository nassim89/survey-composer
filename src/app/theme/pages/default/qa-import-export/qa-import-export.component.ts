import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { QuestionsService } from '../../../../_services/questions.service';
import { AnswersService } from '../../../../_services/answers.service';
import { ImportationMessage } from '../../../../dto/filesIO/importation-message';
import { saveAs } from 'file-saver';

@Component({
    selector: 'app-qa-import-export',
    templateUrl: 'qa-import-export.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./qa-import-export.component.css']
})
export class QAImportExportComponent implements OnInit {
    constructor(private _qnsService: QuestionsService, private _ansService: AnswersService) {}

    questionsImportFile: File = null;
    answersImportFile: File = null;
    importationMessage: ImportationMessage = null;
    error: string;
    importPressed = false;

    ngOnInit() {}

    importQuestions() {
        this.importPressed = true;
        if (this.questionsImportFile) {
            this.importationMessage = null;
            this.error = null;
            this._qnsService.uploadQuestions(this.questionsImportFile).subscribe(
                message => {
                    this.importationMessage = message;
                    this.importPressed = false;
                },
                error => {
                    console.log(error);
                    if (error && error.error && error.error.message) {
                        this.error = error.error.message;
                    }
                    this.importPressed = false;
                }
            );
        }
    }

    importAnswers() {
        if (this.answersImportFile) {
            console.log('pressed');
            this.importationMessage = null;
            this.error = null;
            this._ansService.uploadAnswers(this.answersImportFile).subscribe(
                message => {
                    this.importationMessage = message;
                    this.importPressed = false;
                },
                error => {
                    if (error && error.error && error.error.message) {
                        this.error = error.error.message;
                    }
                    this.importPressed = false;
                }
            );
        }
    }

    exportQuestions() {
        this._qnsService.downloadQuestions().subscribe(data => {
            saveAs(data, `questions_all_${Date.now()}`, { type: 'application/vnd.ms-excel' });
        });
    }

    exportAnswers() {
        this._ansService.downloadAnswers().subscribe(data => {
            saveAs(data, `answers_all_${Date.now()}`, { type: 'application/vnd.ms-excel' });
        });
    }

    getQuestionsFile(event) {
        this.questionsImportFile = event[0];
    }

    getAnswersFile(event) {
        this.answersImportFile = event[0];
    }

    clearResults() {
        this.importationMessage = null;
        this.error = null;
    }
}
