import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageComponent } from './page.component';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { QuestionsService } from '../../../../_services/questions.service';
import { SettingsService } from '../../../../_services/settings.service';
import { MessageService } from '../../../../_services/message.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { AnswersService } from '../../../../_services/answers.service';
import { QuestionDisplayModule } from '../question-display/question-display.module';
import { TagInputModule } from 'ngx-chips';

@NgModule({
    imports: [
        TagInputModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgSelectModule,
        QuestionDisplayModule
    ],
    exports: [PageComponent],
    declarations: [PageComponent],
    providers: [QuestionsService, AnswersService, SettingsService, MessageService]
})
export class PageModule { }
