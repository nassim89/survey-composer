import { AnswerTranslationDTO } from "../../../../dto/answers/output/answer-translation-dto";

export interface AnswerSearchResult {
    checked: boolean;
    translation: AnswerTranslationDTO
}