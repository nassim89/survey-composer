import { Component, OnInit, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { CreateSection } from '../../../../dto/questionnaires/input/create-section';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { QuestionsService } from '../../../../_services/questions.service';
import { AnswersService } from '../../../../_services/answers.service';
import { LabelDTO } from '../../../../dto/questions/output/label-dto';
import { QuestionCategoryDTO } from '../../../../dto/questions/output/question-category-dto';
import { AnswerCategoryDTO } from '../../../../dto/answers/output/answer-category-dto';
import { QuestionDTO } from '../../../../dto/questions/output/question-dto';
import { CreateQuestion } from '../../../../dto/questions/input/create-question';
import { QA } from '../../../../models/local/qa';
import { CreateAnswer } from '../../../../dto/answers/input/create-answer';
import { AnswerDTO } from '../../../../dto/answers/output/answer-dto';
import { OptionDisplayType } from '../../../../models/enums/display-type.enum';
import { QuestionTranslationDTO } from '../../../../dto/questions/output/question-translation-dto';
import { AnswerTranslationDTO } from '../../../../dto/answers/output/answer-translation-dto';
import { QuestionSearchResult } from './question-search-result.model';
import { AnswerSearchResult } from './answer-search-result.model';

@Component({
    selector: 'app-page',
    templateUrl: 'page.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {
    @Output()
    deleted = new EventEmitter();
    _page: CreateSection;
    pageForm: FormGroup;
    addQuestionFormGroup: FormGroup;
    addAnswerFormGroup: FormGroup;
    questionsSearchForm: FormGroup;
    answersSearchForm: FormGroup;
    labels: LabelDTO[];
    qcats: QuestionCategoryDTO[];
    acats: AnswerCategoryDTO[];
    questions: QuestionDTO[];
    answers: AnswerDTO[];
    currentSection: CreateSection;
    questionsSearchResult: QuestionSearchResult[];
    answersSearchResult: AnswerSearchResult[];
    selectedQuestionsTranslations: QuestionTranslationDTO[];
    selectedAnswersTranslations: AnswerTranslationDTO[];
    searchAnswersPressed = false;
    searchQuestionsPressed = false;
    addAnswersPressed = false;
    addQuestionsPressed = false;

    constructor(
        private builder: FormBuilder,
        private _qnsService: QuestionsService,
        private _ansService: AnswersService
    ) {}

    ngOnInit() {
        this.questions = new Array<QuestionDTO>();
        this.answers = new Array<AnswerDTO>();
        this.questionsSearchResult = new Array<QuestionSearchResult>();
        this.selectedQuestionsTranslations = new Array<QuestionTranslationDTO>();

        this._qnsService.findLabels().subscribe(labels => {
            this.labels = labels.filter(l => !l.isDeleted);
        });
        this._qnsService.findQuestionCategories(false).subscribe(qcategories => {
            this.qcats = qcategories.filter(qc => !qc.isDeleted);
        });

        this._ansService.findAnswerCategories(false).subscribe(acategories => {
            this.acats = acategories.filter(ac => !ac.isDeleted);
        });

        this.pageForm = this.builder.group({
            pageTitle: [this.page.title, Validators.required]
        });

        this.addQuestionFormGroup = this.builder.group({
            text: ['', Validators.required],
            qcategory: [null, Validators.required],
            labels: [],
            description: [''],
            comment: ['']
        });

        this.addAnswerFormGroup = this.builder.group({
            text: ['', Validators.required],
            acategory: [null, Validators.required],
            description: [''],
            comment: ['']
        });

        this.questionsSearchForm = this.builder.group({
            searchTerms: [''],
            categoriesIds: [null],
            labels: []
        });

        this.answersSearchForm = this.builder.group({
            searchTerms: [''],
            categoriesIds: [null]
        });
    }

    @Input()
    set page(page: CreateSection) {
        this._page = page;
    }

    get page(): CreateSection {
        return this._page;
    }

    delete() {
        this.deleted.emit(this.page);
    }

    addQuestion() {
        this.addQuestionsPressed = true;
        if (this.addQuestionFormGroup.invalid) {
            this.addQuestionsPressed = false;
            return;
        } else {
            let text: string = this.addQuestionFormGroup.get('text').value;
            const description: string = this.addQuestionFormGroup.get('description').value;
            const comment: string = this.addQuestionFormGroup.get('comment').value;
            let labels = null;

            if (this.addQuestionFormGroup.get('labels').value) {
                labels = (<LabelDTO[]>this.addQuestionFormGroup.get('labels').value).map(
                    label => label.labelId
                );
            }

            const categoryId = this.addQuestionFormGroup.get('qcategory').value;

            const input = new CreateQuestion(text, categoryId, description, comment, labels);
            this._qnsService.createQuestion(input).subscribe(
                question => {
                    if (!this.page.children) {
                        this.page.children = new Array<CreateSection>();
                    }

                    this.questions.push(question);
                    const translation = question.versions[0].translations[0];
                    const section = new CreateSection(
                        '',
                        OptionDisplayType.RADIOBUTTON,
                        null,
                        null,
                        this.page.children.length + 1,
                        null,
                        null
                    );
                    section.qa = new QA(translation);

                    this.page.children.push(section);
                    this.rearrangeSectionNumber(this.page.children);
                    this.addQuestionFormGroup.reset();
                    this.addQuestionsPressed = false;
                },
                error => {
                    console.log(error);
                    this.addQuestionsPressed = false;
                }
            );
        }
    }

    moveUp(elem: CreateSection) {
        if (this.page.children.length <= 1) {
            return;
        }

        const index = this.page.children.findIndex(
            section =>
                section.qa.question.questionTranslationId === elem.qa.question.questionTranslationId
        );

        // Section is the first element
        if (index === 0) {
            const section = this.page.children.shift();
            this.page.children.push(section);
        } else {
            // Section is not the first element
            const section = this.page.children[index];
            this.page.children[index] = this.page.children[index - 1];
            this.page.children[index - 1] = section;
        }

        this.rearrangeSectionNumber(this.page.children);
    }

    moveDown(elem: CreateSection) {
        if (this.page.children.length <= 1) {
            return;
        }

        const index = this.page.children.findIndex(
            section =>
                section.qa.question.questionTranslationId === elem.qa.question.questionTranslationId
        );

        // Section is the first element
        if (index === this.page.children.length - 1) {
            const section = this.page.children.pop();
            this.page.children.unshift(section);
        } else {
            // Section is not the first element
            const section = this.page.children[index];
            this.page.children[index] = this.page.children[index + 1];
            this.page.children[index + 1] = section;
        }

        this.rearrangeSectionNumber(this.page.children);
    }

    deleteSection(elem: CreateSection) {
        this.page.children = this.page.children.filter(
            section =>
                section.qa.question.questionTranslationId !== elem.qa.question.questionTranslationId
        );

        this.rearrangeSectionNumber(this.page.children);
    }

    assignSection(section: CreateSection) {
        this.currentSection = section;
    }

    addAnswer() {
        this.addAnswersPressed = true;
        if (this.addAnswerFormGroup.invalid) {
            this.addAnswersPressed = false;
            return;
        } else {
            let text: string = this.addAnswerFormGroup.get('text').value;
            const description: string = this.addAnswerFormGroup.get('description').value;
            const comment: string = this.addAnswerFormGroup.get('comment').value;
            const categoryId: number = this.addAnswerFormGroup.get('acategory').value;

            const input = new CreateAnswer(text, categoryId, description, comment);
            this._ansService.createAnswer(input).subscribe(
                answer => {
                    if (answer) {
                        this.answers.push(answer);
                        const translation = answer.versions[0].translations[0];
                        this.currentSection.qa.addAnswer(translation);
                        this.addAnswerFormGroup.reset();
                    }
                    this.addAnswersPressed = false;
                },
                error => {
                    this.addAnswersPressed = false;
                    console.log(error);
                }
            );
        }
    }

    rearrangeSectionNumber(sections: CreateSection[]) {
        for (let i = 0; i < sections.length; i++) {
            sections[i].sectionNumber = i;
        }
    }

    searchQuestions() {
        this.searchQuestionsPressed = true;
        this.selectedQuestionsTranslations = [];
        this.questionsSearchResult = [];
        if (this.questionsSearchForm.invalid) {
            this.searchQuestionsPressed = false;
            return;
        } else {
            let searchTerms: string[] = [];
            for (let input of this.questionsSearchForm.value.searchTerms) {
                searchTerms.push(input.value);
            }
            let categories: number[] = this.questionsSearchForm.value.categoriesIds;
            let labels: number[] = this.questionsSearchForm.value.labels;
            let languageId: number[] = [1]; // English Language ID (TODO: use a enum)
            this._qnsService
                .findQuestionTanslationByFilters(null, categories, labels, languageId, searchTerms)
                .subscribe(
                    translations => {
                        let sortedTranslations = translations
                            .filter(t => !t.isDeleted)
                            .sort((a, b) => a.text.localeCompare(b.text));
                        for (let t of sortedTranslations) {
                            this.questionsSearchResult.push({ checked: false, translation: t });
                        }
                        this.searchQuestionsPressed = false;
                    },
                    error => {
                        console.log(error);
                        this.searchQuestionsPressed = false;
                    }
                );
        }
    }

    searchAnswers() {
        this.searchAnswersPressed = true;
        this.selectedAnswersTranslations = [];
        this.answersSearchResult = [];
        if (this.answersSearchForm.invalid) {
            this.searchAnswersPressed = false;
            return;
        } else {
            let searchTerms: string[] = [];
            for (let input of this.answersSearchForm.value.searchTerms) {
                searchTerms.push(input.value);
            }
            let categories: number[] = this.answersSearchForm.value.categoriesIds;
            let languageId: number[] = [1]; // English Language ID (TODO: use a enum)
            this._ansService
                .findAnswerTanslationByFilters(null, categories, languageId, searchTerms)
                .subscribe(
                    translations => {
                        let sortedTranslations = translations
                            .filter(t => !t.isDeleted)
                            .sort((a, b) => a.text.localeCompare(b.text));
                        for (let t of sortedTranslations) {
                            this.answersSearchResult.push({ checked: false, translation: t });
                        }
                        this.searchAnswersPressed = false;
                    },
                    error => {
                        console.log(error);
                        this.searchAnswersPressed = false;
                    }
                );
        }
    }

    toggleQuestionTranslation(result: QuestionSearchResult) {
        result.checked = !result.checked;

        if (result.checked) {
            this.selectedQuestionsTranslations.push(result.translation);
        } else {
            this.selectedQuestionsTranslations = this.selectedQuestionsTranslations.filter(
                t => t.questionTranslationId !== result.translation.questionTranslationId
            );
        }
    }

    toggleAnswerTranslation(result: AnswerSearchResult) {
        result.checked = !result.checked;

        if (result.checked) {
            this.selectedAnswersTranslations.push(result.translation);
        } else {
            this.selectedAnswersTranslations = this.selectedAnswersTranslations.filter(
                t => t.answerTranslationId !== result.translation.answerTranslationId
            );
        }
    }

    selectAllQuestionsFromSearch() {
        this.selectedQuestionsTranslations = [];
        for (let result of this.questionsSearchResult) {
            result.checked = true;
            this.selectedQuestionsTranslations.push(result.translation);
        }
    }

    unselectAllQuestionsFromSearch() {
        for (let result of this.questionsSearchResult) {
            result.checked = false;
        }
        this.selectedQuestionsTranslations = [];
    }

    selectAllAnswersFromSearch() {
        this.selectedAnswersTranslations = [];
        for (let result of this.answersSearchResult) {
            result.checked = true;
            this.selectedAnswersTranslations.push(result.translation);
        }
    }

    unselectAllAnswersFromSearch() {
        for (let result of this.answersSearchResult) {
            result.checked = false;
        }
        this.selectedAnswersTranslations = [];
    }

    addQuestionsFromSearch() {
        for (let questionTranslation of this.selectedQuestionsTranslations) {
            if (!this.page.children) {
                this.page.children = new Array<CreateSection>();
            }

            if (
                this.page.children.find(
                    c =>
                        c.qa.question.questionTranslationId ===
                        questionTranslation.questionTranslationId
                ) === undefined
            ) {
                const section = new CreateSection(
                    '',
                    OptionDisplayType.RADIOBUTTON,
                    null,
                    null,
                    this.page.children.length + 1,
                    null,
                    null
                );
                section.qa = new QA(questionTranslation);

                this.page.children.push(section);
                this.rearrangeSectionNumber(this.page.children);
            }
        }
    }

    addAnswersFromSearch() {
        for (let answerTranslation of this.selectedAnswersTranslations) {
            this.currentSection.qa.addAnswer(answerTranslation);
        }
    }
}
