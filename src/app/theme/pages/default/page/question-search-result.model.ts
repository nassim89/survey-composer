import { QuestionTranslationDTO } from "../../../../dto/questions/output/question-translation-dto";

export interface QuestionSearchResult {
    checked: boolean;
    translation: QuestionTranslationDTO
}