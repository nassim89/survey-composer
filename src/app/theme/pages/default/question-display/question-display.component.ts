import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { CreateSection } from '../../../../dto/questionnaires/input/create-section';
import { AnswerTranslationDTO } from '../../../../dto/answers/output/answer-translation-dto';

@Component({
    selector: 'app-question-display',
    templateUrl: 'question-display.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./question-display.component.css']
})
export class QuestionDisplayComponent implements OnInit {
    _section: CreateSection;

    constructor() { }

    ngOnInit() { }

    @Input()
    set section(section: CreateSection) {
        this._section = section;
    }

    get section(): CreateSection {
        return this._section;
    }

    moveUp(elem: AnswerTranslationDTO) {
        if (this.section.qa.answers.length <= 1) {
            return;
        }

        const index = this.section.qa.answers.findIndex(
            answer => answer.answerTranslationId === elem.answerTranslationId
        );

        // Answer is the first element
        if (index === 0) {
            const answer = this.section.qa.answers.shift();
            this.section.qa.answers.push(answer);
        } else {
            // Answer is not the first element
            const answer = this.section.qa.answers[index];
            this.section.qa.answers[index] = this.section.qa.answers[index - 1];
            this.section.qa.answers[index - 1] = answer;
        }
    }

    moveDown(elem: AnswerTranslationDTO) {
        if (this.section.qa.answers.length <= 1) {
            return;
        }

        const index = this.section.qa.answers.findIndex(
            answer => answer.answerTranslationId === elem.answerTranslationId
        );

        // Answer is the first element
        if (index === this.section.qa.answers.length - 1) {
            const answer = this.section.qa.answers.pop();
            this.section.qa.answers.unshift(answer);
        } else {
            // Answer is not the first element
            const answer = this.section.qa.answers[index];
            this.section.qa.answers[index] = this.section.qa.answers[index + 1];
            this.section.qa.answers[index + 1] = answer;
        }
    }

    deleteAnswer(elem: AnswerTranslationDTO) {
        this.section.qa.answers = this.section.qa.answers.filter(
            answer => answer.answerTranslationId !== elem.answerTranslationId
        );
    }
}
