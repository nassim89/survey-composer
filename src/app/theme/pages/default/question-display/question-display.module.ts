import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionDisplayComponent } from './question-display.component';
import { FormsModule, ReactiveFormsModule } from '../../../../../../node_modules/@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, NgSelectModule],
    exports: [QuestionDisplayComponent],
    declarations: [QuestionDisplayComponent],
    providers: []
})
export class QuestionDisplayModule { }
