import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { JwtService } from '../../../auth/_services/jwt.service';
import { PlatformUserDTO } from '../../../dto/users/output/platform-user-dto';

declare let mLayout: any;
@Component({
    selector: 'app-header-nav',
    templateUrl: './header-nav.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
    currentUser: PlatformUserDTO;

    constructor(private _jwtService: JwtService) {}
    ngOnInit() {
        this.currentUser = Object.assign({}, this._jwtService.getCurrentUser());
    }
    ngAfterViewInit() {
        mLayout.initHeader();
    }
}
