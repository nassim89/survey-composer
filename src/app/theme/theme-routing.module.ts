import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/_guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: ThemeComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                loadChildren: './pages/default/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'categories/questions',
                loadChildren:
                    './pages/default/categories-questions/categories-questions.module#CategoriesQuestionsModule'
            },
            {
                path: 'categories/answers',
                loadChildren:
                    './pages/default/categories-answers/categories-answers.module#CategoriesAnswersModule'
            },
            {
                path: 'category/:questionCategoryId/questions',
                loadChildren: './pages/default/questions/questions.module#QuestionsModule'
            },
            {
                path: 'category/:answerCategoryId/answers',
                loadChildren: './pages/default/answers/answers.module#AnswersModule'
            },
            {
                path: 'questionnaires',
                loadChildren:
                    './pages/default/questionnaires/list-view/questionnaire-list-view.module#QuestionnaireListViewModule'
            },
            {
                path: 'questionnaires/build',
                loadChildren:
                    './pages/default/questionnaires/build/build-questionnaire.module#BuildQuestionnaireModule'
            },
            {
                path: 'questionnaires/build/:questionnaireId',
                loadChildren:
                    './pages/default/questionnaires/build/build-questionnaire.module#BuildQuestionnaireModule'
            },
            {
                path: 'questionnaires/:questionnaireId/translate',
                loadChildren:
                    './pages/default/questionnaires/translate/translate-questionnaire.module#TranslateQuestionnaireModule'
            },
            {
                path: 'questionnaires/:questionnaireId/view',
                loadChildren:
                    './pages/default/questionnaires/view/view-questionnaire.module#ViewQuestionnaireModule'
            },
            {
                path: 'labels',
                loadChildren: './pages/default/labels/labels.module#LabelsModule'
            },
            {
                path: 'languages',
                loadChildren: './pages/default/languages/languages.module#LanguagesModule'
            },
            {
                path: 'backup',
                loadChildren:
                    './pages/default/qa-import-export/qa-import-export.module#QAImportExportModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ThemeRoutingModule {}
