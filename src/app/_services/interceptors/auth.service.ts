import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { JwtService } from '../../auth/_services/jwt.service';

@Injectable()
export class AuthService implements HttpInterceptor {
    constructor(private jwtService: JwtService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this.jwtService.getToken()) {
            if (req.url.endsWith('file/upload')) {
                req = req.clone({
                    setHeaders: {
                        'Authorization': 'Bearer ' + this.jwtService.getToken()
                    }
                });
            } else {
                req = req.clone({
                    setHeaders: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + this.jwtService.getToken()
                    }
                });
            }
        } else {
            req = req.clone({
                setHeaders: {
                    'Content-Type': 'application/json'
                }
            });
        }
        return next.handle(req);
    }
}
