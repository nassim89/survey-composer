import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { MessageService } from './message.service';
import { CreateQuestion } from '../dto/questions/input/create-question';
import { UpdateQuestion } from '../dto/questions/input/update-question';
import { CreateCategory } from '../dto/questions/input/create-category';
import { UpdateCategory } from '../dto/questions/input/update-category';
import { CreateQuestionVersion } from '../dto/questions/input/create-question-version';
import { UpdateQuestionVersion } from '../dto/questions/input/update-question-version';
import { CreateQuestionTranslation } from '../dto/questions/input/create-question-translation';
import { UpdateQuestionTranslation } from '../dto/questions/input/update-question-translation';
import { CreateLabel } from '../dto/questions/input/create-label';
import { UpdateLabel } from '../dto/questions/input/update-label';
import { ImportationMessage } from '../dto/filesIO/importation-message';
import { QuestionDTO } from '../dto/questions/output/question-dto';
import { QuestionCategoryDTO } from '../dto/questions/output/question-category-dto';
import { QuestionVersionDTO } from '../dto/questions/output/question-version-dto';
import { QuestionTranslationDTO } from '../dto/questions/output/question-translation-dto';
import { LabelDTO } from '../dto/questions/output/label-dto';

@Injectable()
export class QuestionsService {
    private groupsUrl = 'http://109.135.0.135/survey-composer/api/questions/categories';
    private questionsUrl = 'http://109.135.0.135/survey-composer/api/questions';

    constructor(private http: HttpClient, private messageService: MessageService) {}

    /**
     * findQuestions
     * @param questionCategoryId
     * @param labelId
     */
    findQuestions(questionCategoryId?: number, labelId?: number): Observable<QuestionDTO[]> {
        const url = `${this.questionsUrl}`;
        let params;
        if (labelId === undefined && questionCategoryId) {
            params = {
                questionCategoryId: '' + questionCategoryId
            };
        } else if (labelId && questionCategoryId === undefined) {
            params = {
                labelId: '' + labelId
            };
        } else if (labelId === undefined && questionCategoryId === undefined) {
            params = {};
        } else {
            params = {
                questionCategoryId: '' + questionCategoryId,
                labelId: '' + labelId
            };
        }

        return this.http.get<QuestionDTO[]>(url, { params: params }).pipe(
            tap(questions => this.log(`Fetched ${questions.length} questions.`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionById
     * @param questionId
     */
    findQuestionById(questionId: number): Observable<QuestionDTO> {
        const url = `${this.questionsUrl}/${questionId}`;
        return this.http.get<QuestionDTO>(url).pipe(
            tap(question => this.log(`Fetched question w/ id = ${question.questionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createQuestion
     * @param input
     */
    createQuestion(input: CreateQuestion): Observable<QuestionDTO> {
        const url = `${this.questionsUrl}`;
        return this.http.post<QuestionDTO>(url, input).pipe(
            tap(question => this.log(`Added a question w/ id : ${question.questionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateQuestion
     * @param questionId
     * @param input
     */
    updateQuestion(questionId: number, input: UpdateQuestion): Observable<QuestionDTO> {
        const url = `${this.questionsUrl}/${questionId}`;
        return this.http.put<QuestionDTO>(url, input).pipe(
            tap(question => this.log(`Updated a question w/ id : ${question.questionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteQuestion
     * @param questionId
     */
    deleteQuestion(questionId: number): Observable<QuestionDTO> {
        const url = `${this.questionsUrl}/${questionId}`;
        return this.http.delete<QuestionDTO>(url).pipe(
            tap(q => this.log(`Deleted a question w/ id : ${q.questionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionCategories
     * @param includeNested
     */
    findQuestionCategories(includeNested?: boolean): Observable<QuestionCategoryDTO[]> {
        const url = `${this.groupsUrl}${
            includeNested ? '?includeNested=true' : '?includeNested=false'
        }`;
        return this.http.get<QuestionCategoryDTO[]>(url).pipe(
            tap(categories => this.log(`Fetched ${categories.length} categories`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionCategoriesById
     * @param questionCategoryId
     */
    findQuestionCategoriesById(questionCategoryId: number): Observable<QuestionCategoryDTO> {
        const url = `${this.groupsUrl}/${questionCategoryId}`;
        return this.http.get<QuestionCategoryDTO>(url).pipe(
            tap(category => this.log(`Fetched category w/ id=${category.questionCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createQuestionCategory
     * @param input
     */
    createQuestionCategory(input: CreateCategory | Object): Observable<QuestionCategoryDTO> {
        const url = `${this.groupsUrl}`;
        return this.http.post<QuestionCategoryDTO>(url, input).pipe(
            tap(category => this.log(`Created category w/ id=${category.questionCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateQuestionCategory
     * @param questionCategoryId
     * @param input
     */
    updateQuestionCategory(
        questionCategoryId: number,
        input: UpdateCategory
    ): Observable<QuestionCategoryDTO> {
        const url = `${this.groupsUrl}/${questionCategoryId}`;
        return this.http.put<QuestionCategoryDTO>(url, input).pipe(
            tap(category => this.log(`Updated category w/ id=${category.questionCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteQuestionCategory
     * @param questionCategoryId
     */
    deleteQuestionCategory(questionCategoryId: number): Observable<QuestionCategoryDTO> {
        const url = `${this.groupsUrl}/${questionCategoryId}`;
        return this.http.delete<QuestionCategoryDTO>(url).pipe(
            tap(qc => this.log(`Deleted category w/ id=${qc.questionCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionVersions
     * @param questionId
     */
    findQuestionVersions(questionId: number): Observable<QuestionVersionDTO[]> {
        const url = `${this.questionsUrl}/${questionId}/versions`;
        return this.http.get<QuestionVersionDTO[]>(url).pipe(
            tap(versions => this.log(`Fetched ${versions.length} versions`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionVersionById
     * @param questionId
     * @param questionVersionId
     */
    findQuestionVersionById(
        questionId: number,
        questionVersionId: number
    ): Observable<QuestionVersionDTO> {
        const url = `${this.questionsUrl}/${questionId}/versions/${questionVersionId}`;
        return this.http.get<QuestionVersionDTO>(url).pipe(
            tap(version => this.log(`Fetched version w/ id=${version.questionVersionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createVersion
     * @param questionId
     * @param input
     */
    createVersion(
        questionId: number,
        input: CreateQuestionVersion
    ): Observable<QuestionVersionDTO> {
        const url = `${this.questionsUrl}/${questionId}/versions`;
        return this.http.post<QuestionVersionDTO>(url, input).pipe(
            tap((version: QuestionVersionDTO) =>
                this.log(`Created a version for question w/ id : ${version.questionVersionId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateQuestionVersion
     * @param questionId
     * @param questionVersionId
     * @param input
     */
    updateQuestionVersion(
        questionId: number,
        questionVersionId: number,
        input: UpdateQuestionVersion
    ): Observable<QuestionVersionDTO> {
        const url = `${this.questionsUrl}/${questionId}/versions/${questionVersionId}`;
        return this.http.put<QuestionVersionDTO>(url, input).pipe(
            tap((version: QuestionVersionDTO) =>
                this.log(`Updated a version w/ id : ${version.questionVersionId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteQuestionVersion
     * @param questionId
     * @param questionVersionId
     */
    deleteQuestionVersion(
        questionId: number,
        questionVersionId: number
    ): Observable<QuestionVersionDTO> {
        const url = `${this.questionsUrl}/${questionId}/versions/${questionVersionId}`;
        return this.http.delete<QuestionVersionDTO>(url).pipe(
            tap(qv => this.log(`Deleted a version w/ id : ${qv.questionVersionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionTranslations
     * @param questionId
     * @param questionVersionId
     */
    findQuestionTranslations(
        questionId: number,
        questionVersionId: number
    ): Observable<QuestionTranslationDTO[]> {
        const url = `${this.questionsUrl}/${questionId}/versions/${questionVersionId}/translations`;
        return this.http.get<QuestionTranslationDTO[]>(url).pipe(
            tap(translations => this.log(`Fetched ${translations.length} translations`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionTanslationById
     * @param questionId
     * @param questionVersionId
     * @param questionTranslationId
     */
    findQuestionTanslationById(
        questionId: number,
        questionVersionId: number,
        questionTranslationId: number
    ): Observable<QuestionTranslationDTO> {
        const url = `${
            this.questionsUrl
        }/${questionId}/versions/${questionVersionId}/translations/${questionTranslationId}`;
        return this.http.get<QuestionTranslationDTO>(url).pipe(
            tap(translation =>
                this.log(`Fetched translation w/ id=${translation.questionTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createQuestionTranslation
     * @param questionId
     * @param questionVersionId
     * @param input
     */
    createQuestionTranslation(
        questionId: number,
        questionVersionId: number,
        input: CreateQuestionTranslation
    ): Observable<QuestionTranslationDTO> {
        const url = `${this.questionsUrl}/${questionId}/versions/${questionVersionId}/translations`;
        return this.http.post<QuestionTranslationDTO>(url, input).pipe(
            tap(translation =>
                this.log(`Created translation w/ id=${translation.questionTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateQuestionTranslation
     * @param questionId
     * @param questionVersionId
     * @param questionTranslationId
     * @param input
     */
    updateQuestionTranslation(
        questionId: number,
        questionVersionId: number,
        questionTranslationId: number,
        input: UpdateQuestionTranslation
    ): Observable<QuestionTranslationDTO> {
        const url = `${
            this.questionsUrl
        }/${questionId}/versions/${questionVersionId}/translations/${questionTranslationId}`;
        return this.http.put<QuestionTranslationDTO>(url, input).pipe(
            tap(translation =>
                this.log(`Updated translation w/ id=${translation.questionTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteQuestionTranslation
     * @param questionId
     * @param questionVersionId
     * @param questionTranslationId
     */
    deleteQuestionTranslation(
        questionId: number,
        questionVersionId: number,
        questionTranslationId: number
    ): Observable<QuestionTranslationDTO> {
        const url = `${
            this.questionsUrl
        }/${questionId}/versions/${questionVersionId}/translations/${questionTranslationId}`;
        return this.http.delete<QuestionTranslationDTO>(url).pipe(
            tap(qt => this.log(`Deleted translation w/ id=${qt.questionTranslationId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findQuestionTanslationByFilters
     * @param categoryId
     * @param labelId
     * @param languageId
     * @param term
     */
    findQuestionTanslationByFilters(
        versionId?: number[],
        categoryId?: number[],
        labelId?: number[],
        languageId?: number[],
        terms?: string[]
    ): Observable<QuestionTranslationDTO[]> {
        let url = `${this.questionsUrl}/translations?`;

        if (versionId) {
            const versionIds = versionId.join(',');
            url += `versionId=${versionIds}&`;
        }

        if (categoryId) {
            const categories = categoryId.join(',');
            url += `categoryId=${categories}&`;
        }

        if (labelId) {
            const labels = labelId.join(',');
            url += `labelId=${labels}&`;
        }

        if (languageId) {
            const languages = languageId.join(',');
            url += `languageId=${languages}&`;
        }

        if (terms) {
            const searchTerms = terms.join(',');
            url += `term=${searchTerms}&`;
        }

        return this.http.get<QuestionTranslationDTO[]>(url).pipe(
            tap(translations => this.log(`Fetched ${translations.length} translations`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findLabels
     */
    findLabels(): Observable<LabelDTO[]> {
        const url = `${this.questionsUrl}/labels`;
        return this.http.get<LabelDTO[]>(url).pipe(
            tap(labels => this.log(`Fetched ${labels.length} labels`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findLabelById
     * @param labelId
     */
    findLabelById(labelId: number): Observable<LabelDTO> {
        const url = `${this.questionsUrl}/labels/${labelId}`;
        return this.http.get<LabelDTO>(url).pipe(
            tap(label => this.log(`Fetched label w/ id ${label.labelId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createLabel
     * @param input
     */
    createLabel(input: CreateLabel): Observable<LabelDTO> {
        const url = `${this.questionsUrl}/labels`;
        return this.http.post<LabelDTO>(url, input).pipe(
            tap(label => this.log(`Created label w/ id=${label.labelId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateLabel
     * @param labelId
     * @param input
     */
    updateLabel(labelId: number, input: UpdateLabel): Observable<LabelDTO> {
        const url = `${this.questionsUrl}/labels/${labelId}`;
        return this.http.put<LabelDTO>(url, input).pipe(
            tap(label => this.log(`Updated label w/ id=${label.labelId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteLabel
     * @param labelId
     */
    deleteLabel(labelId: number): Observable<LabelDTO> {
        const url = `${this.questionsUrl}/labels/${labelId}`;
        return this.http.delete<LabelDTO>(url).pipe(
            tap(l => this.log(`Deleted label w/ id=${l.labelId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     *
     * @param fileToUpload
     */
    uploadQuestions(fileToUpload: File): Observable<ImportationMessage> {
        const url = `${this.questionsUrl}/file/upload`;
        const formData: FormData = new FormData();
        formData.append('importationFile', fileToUpload, fileToUpload.name);
        return this.http.post<ImportationMessage>(url, formData).pipe(
            tap(b => this.log(`Uploaded questions : ${b}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    downloadQuestions() {
        const url = `${this.questionsUrl}/file/download`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server error');
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.status + ': ' + error.error.message}`);
            // Let the app keep running by returning an empty result.
            return Observable.throw(error || 'Server error');
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add(message);
    }
}
