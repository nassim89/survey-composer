import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { CreateQuestionnaireGroup } from '../dto/questionnaires/input/create-questionnaire-group';
import { CreateQuestionnaire } from '../dto/questionnaires/input/create-questionnaire';
import { UpdateQuestionnaire } from '../dto/questionnaires/input/update-questionnaire';
import { UpdateQuestionnaireGroup } from '../dto/questionnaires/input/update-questionnaire-group';
import { CreateTitleTranslation } from '../dto/questionnaires/input/create-title-translation';
import { UpdateTitleTranslation } from '../dto/questionnaires/input/update-title-translation';
import { QuestionnaireDTO } from '../dto/questionnaires/output/questionnaire-dto';
import { QuestionnaireGroupDTO } from '../dto/questionnaires/output/questionnaire-group-dto';
import { TitleDTO } from '../dto/questionnaires/output/title-dto';
import { TitleTranslationDTO } from '../dto/questionnaires/output/title-translation-dto';
import { ResponseContentType, RequestOptions } from '@angular/http';

@Injectable()
export class QuestionnairesService {
    private questionnairesUrl = 'http://109.135.0.135/survey-composer/api/questionnaire';

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * findQuestionnaires
     */
    findQuestionnaires(): Observable<QuestionnaireDTO[]> {
        const url = `${this.questionnairesUrl}`;
        return this.http.get<QuestionnaireDTO[]>(url).pipe(
            tap(questionnaires => this.log(`Fetched ${questionnaires.length} questionnaires.`)),
            catchError(this.handleError<QuestionnaireDTO[]>('findQuestionnaires'))
        );
    }

    /**
     * findQuestionnaireById
     * @param questionnaireId
     */
    findQuestionnaireById(questionnaireId: number): Observable<QuestionnaireDTO> {
        const url = `${this.questionnairesUrl}/${questionnaireId}`;
        return this.http.get<QuestionnaireDTO>(url).pipe(
            tap(questionnaire =>
                this.log(`Fetched questionnaire w/ id =${questionnaire.questionnaireId}`)
            ),
            catchError(this.errorHandler)
        );
    }

    /**
     * findQuestionnairesByQuestionnaireTranslationGroupId
     * @param questionnaireTranslationGroupId
     */
    findQuestionnairesByQuestionnaireTranslationGroupId(
        questionnaireTranslationGroupId: number
    ): Observable<QuestionnaireDTO[]> {
        const url = `${this.questionnairesUrl}/fromgroup/${questionnaireTranslationGroupId}`;
        return this.http.get<QuestionnaireDTO[]>(url).pipe(
            tap(questionnaires =>
                this.log(
                    `Fetched questionnaires for group with id = ${questionnaireTranslationGroupId}`
                )
            ),
            catchError(
                this.handleError<QuestionnaireDTO[]>(
                    'findQuestionnairesByQuestionnaireTranslationGroupId'
                )
            )
        );
    }

    /**
     * createQuestionnaire
     * @param input
     */
    createQuestionnaire(input: CreateQuestionnaire): Observable<QuestionnaireDTO> {
        const url = `${this.questionnairesUrl}`;
        return this.http.post<QuestionnaireDTO>(url, input).pipe(
            tap(questionnaire =>
                this.log(`Created questionnaire w/ id=${questionnaire.questionnaireId}.`)
            ),
            catchError(this.handleError<QuestionnaireDTO>('createQuestionnaire'))
        );
    }

    /**
     * updateQuestionnaire
     * @param questionnaireId
     * @param input
     */
    updateQuestionnaire(
        questionnaireId: number,
        input: UpdateQuestionnaire
    ): Observable<QuestionnaireDTO> {
        const url = `${this.questionnairesUrl}/${questionnaireId}`;
        return this.http.put<QuestionnaireDTO>(url, input).pipe(
            tap(questionnaire =>
                this.log(`Updated questionnaire w/ id=${questionnaire.questionnaireId}.`)
            ),
            catchError(this.handleError<QuestionnaireDTO>('updateQuestionnaire'))
        );
    }

    /**
     * deleteQuestionnaire
     * @param questionnaireId
     */
    deleteQuestionnaire(questionnaireId: number): Observable<QuestionnaireDTO> {
        const url = `${this.questionnairesUrl}/${questionnaireId}`;
        return this.http.delete<QuestionnaireDTO>(url).pipe(
            tap(q => this.log(`Updated questionnaire w/ id=${q.questionnaireId}`)),
            catchError(this.handleError<QuestionnaireDTO>('deleteQuestionnaire'))
        );
    }

    /**
     * findQuestionnaireGroups
     */
    findQuestionnaireGroups(): Observable<QuestionnaireGroupDTO[]> {
        const url = `${this.questionnairesUrl}/groups`;
        return this.http.get<QuestionnaireGroupDTO[]>(url).pipe(
            tap(groups => this.log(`Fetched ${groups.length} groups.`)),
            catchError(this.handleError<QuestionnaireGroupDTO[]>('findQuestionnaireGroups'))
        );
    }

    /**
     * findQuestionnaireGroupsById
     * @param questionnaireGroupId
     */
    findQuestionnaireGroupsById(questionnaireGroupId: number): Observable<QuestionnaireGroupDTO> {
        const url = `${this.questionnairesUrl}/groups/${questionnaireGroupId}`;
        return this.http.get<QuestionnaireGroupDTO>(url).pipe(
            tap(group => this.log(`Fetched group w/ id=${group.questionnaireGroupId}`)),
            catchError(this.handleError<QuestionnaireGroupDTO>('findQuestionnaireGroupsById'))
        );
    }

    /**
     * createQuestionnaireGroup
     * @param input
     */
    createQuestionnaireGroup(input: CreateQuestionnaireGroup): Observable<QuestionnaireGroupDTO> {
        const url = `${this.questionnairesUrl}/groups`;
        return this.http.post<QuestionnaireGroupDTO>(url, input).pipe(
            tap(group => this.log(`Created group w/ id=${group.questionnaireGroupId}.`)),
            catchError(this.handleError<QuestionnaireGroupDTO>('createQuestionnaireGroup'))
        );
    }

    /**
     * updateQuestionnaireGroups
     * @param questionnaireGroupId
     * @param input
     */
    updateQuestionnaireGroups(
        questionnaireGroupId: number,
        input: UpdateQuestionnaireGroup
    ): Observable<QuestionnaireGroupDTO> {
        const url = `${this.questionnairesUrl}/groups/${questionnaireGroupId}`;
        return this.http.put<QuestionnaireGroupDTO>(url, input).pipe(
            tap(group => this.log(`Updated group w/ id=${group.questionnaireGroupId}`)),
            catchError(this.handleError<QuestionnaireGroupDTO>('updateQuestionnaireGroups'))
        );
    }

    /**
     * deleteQuestionnaireGroups
     * @param questionnaireGroupId
     */
    deleteQuestionnaireGroups(questionnaireGroupId: number): Observable<QuestionnaireGroupDTO> {
        const url = `${this.questionnairesUrl}/groups/${questionnaireGroupId}`;
        return this.http.delete<QuestionnaireGroupDTO>(url).pipe(
            tap(q => this.log(`Deleted group w/ id=${q.questionnaireGroupId}`)),
            catchError(this.handleError<QuestionnaireGroupDTO>('deleteQuestionnaireGroups'))
        );
    }

    /**
     * findTitle
     * @param questionnaireId
     */
    findTitle(questionnaireId: number): Observable<TitleDTO> {
        const url = `${this.questionnairesUrl}/${questionnaireId}/titles`;
        return this.http.get<TitleDTO>(url).pipe(
            tap(title => this.log(`Fetched title w/ id=${title.titleId}`)),
            catchError(this.handleError<TitleDTO>('findTitle'))
        );
    }

    /**
     * createTitleTranslation
     * @param questionnaireId
     * @param CreateTitleTranslation
     */
    createTitleTranslation(
        questionnaireId: number,
        input: CreateTitleTranslation
    ): Observable<TitleTranslationDTO> {
        const url = `${this.questionnairesUrl}/${questionnaireId}/titleTranslations`;
        return this.http.post<TitleTranslationDTO>(url, input).pipe(
            tap(titleTranslation =>
                this.log(`Created translation w/ id=${titleTranslation.titleTranslationId}`)
            ),
            catchError(this.handleError<TitleTranslationDTO>('createTitleTranslation'))
        );
    }

    /**
     * updateTitleTranslation
     * @param questionnaireId
     * @param titleTranslationId
     * @param input
     */
    updateTitleTranslation(
        questionnaireId: number,
        titleTranslationId: number,
        input: UpdateTitleTranslation
    ): Observable<TitleTranslationDTO> {
        const url = `${
            this.questionnairesUrl
            }/${questionnaireId}/titleTranslations/${titleTranslationId}`;
        return this.http.put<TitleTranslationDTO>(url, input).pipe(
            tap(titleTranslation =>
                this.log(`Updated translation w/ id=${titleTranslation.titleTranslationId}`)
            ),
            catchError(this.handleError<TitleTranslationDTO>('updateTitleTranslation'))
        );
    }

    /**
     * deleteTitleTranslation
     * @param questionnaireId
     * @param titleTranslationId
     */
    deleteTitleTranslation(
        questionnaireId: number,
        titleTranslationId: number
    ): Observable<TitleTranslationDTO> {
        const url = `${
            this.questionnairesUrl
            }/${questionnaireId}/titleTranslations/${titleTranslationId}`;
        return this.http.delete<TitleTranslationDTO>(url).pipe(
            tap(tt => this.log(`Deleted translation w/ id=${tt.titleTranslationId}`)),
            catchError(this.handleError<TitleTranslationDTO>('deleteTitleTranslation'))
        );
    }

    downloadQuestionnaire(questionnaireId: number) {
        const url = `${this.questionnairesUrl}/file/download?questionnaireId=${questionnaireId}`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server error');
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            return Observable.throw(error || 'Server error');
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Questions Service : ' + message);
    }
}
