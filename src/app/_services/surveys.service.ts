import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { SurveyDeploymentObject } from '../dto/surveys/survey-deployment-object';
import { DeploySurveyObject } from '../dto/surveys/deploy-survey-object';

const httpOptions = {
    headers: new HttpHeaders(
        {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa('admin:admin')
        }
    )
};

@Injectable()
export class SurveysService {

    private surveysUrl = 'http://109.135.0.135/survey-composer/api/surveys';

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * createSurvey
     * @param input
     */
    createSurvey(input: DeploySurveyObject): Observable<SurveyDeploymentObject> {
        const url = `${this.surveysUrl}`;
        return this.http.post<SurveyDeploymentObject>(url, input).pipe(
            catchError(this.handleError<SurveyDeploymentObject>('createSurvey'))
        );
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Surveys Service : ' + message);
    }

}
