import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { CreateUser } from '../dto/users/input/create-user';
import { UpdateUser } from '../dto/users/input/update-user';
import { PlatformUserDTO } from '../dto/users/output/platform-user-dto';

@Injectable()
export class UsersService {

    isLoggedIn: Boolean = false;
    redirectUrl: string;

    private usersUrl = 'http://109.135.0.135/survey-composer/users';

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * findUsers
     */
    findUsers(): Observable<PlatformUserDTO[]> {
        const url = `${this.usersUrl}`;
        return this.http.get<PlatformUserDTO[]>(url).pipe(
            tap(users => this.log(`Fetched ${users.length} users`)),
            catchError(this.handleError<PlatformUserDTO[]>('findUsers'))
        );
    }

    /**
     * findUserById
     * @param userId
     */
    findUserById(userId: number): Observable<PlatformUserDTO> {
        const url = `${this.usersUrl}/${userId}`;
        return this.http.get<PlatformUserDTO>(url).pipe(
            tap(user => this.log(`Fetched user w/ id=${user.userId}`)),
            catchError(this.handleError<PlatformUserDTO>('findUserById'))
        );
    }

    /**
     * createUser
     * @param input
     */
    createUser(input: CreateUser): Observable<PlatformUserDTO> {
        const url = `${this.usersUrl}`;
        return this.http.post<PlatformUserDTO>(url, input).pipe(
            tap(user => this.log(`Created user w/ id=${user.userId}`)),
            catchError(this.handleError<PlatformUserDTO>('createUser'))
        );
    }

    /**
     * updateUser
     * @param userId
     * @param input
     */
    updateUser(userId: number, input: UpdateUser): Observable<PlatformUserDTO> {
        const url = `${this.usersUrl}/${userId}`;
        return this.http.put<PlatformUserDTO>(url, input).pipe(
            tap(user => this.log(`Updated user w/ id=${user.userId}`)),
            catchError(this.handleError<PlatformUserDTO>('updateUser'))
        );
    }

    /**
     * deleteUser
     * @param userId
     */
    deleteUser(userId: number): Observable<PlatformUserDTO> {
        const url = `${this.usersUrl}/${userId}`;
        return this.http.delete<PlatformUserDTO>(url).pipe(
            tap(user => this.log(`Updated user w/ id=${user.userId}`)),
            catchError(this.handleError<PlatformUserDTO>('deleteUser'))
        );
    }

    /**
     * login
     * @param username
     * @param password
     */
    login(username: string, password: string): Observable<Boolean> {
        const url = `${this.usersUrl}/login`;
        sessionStorage.setItem('ub64', 'Basic ' + btoa(username + ':' + password));

        return this.http.post<Boolean>(url, {}).pipe(
            tap(b => this.log(`${username} logged in.`)),
            tap(b => this.isLoggedIn = b),
            catchError(this.handleError<Boolean>('login'))
        );
    }

    /**
     * logout
     */
    logout(): Observable<Boolean> {
        const url = `${this.usersUrl}/logout`;

        return this.http.post<Boolean>(url, {}).pipe(
            tap(b => this.log(`logged out.`)),
            tap(b => {
                if (b) {
                    localStorage.removeItem('ub64');
                }
            }),
            tap(b => this.isLoggedIn = false),
            catchError(this.handleError<Boolean>('logout'))
        );
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * handleLoginError
     * @param operation
     * @param result
     */
    private handleLoginError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            console.error(error);

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Questions Service : ' + message);
    }

}
