import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { SectionDTO } from '../dto/questionnaires/output/section-dto';
import { CreateSection } from '../dto/questionnaires/input/create-section';
import { UpdateSection } from '../dto/questionnaires/input/update-section';
import { CreateTitleTranslation } from '../dto/questionnaires/input/create-title-translation';
import { UpdateTitleTranslation } from '../dto/questionnaires/input/update-title-translation';
import { TitleDTO } from '../dto/questionnaires/output/title-dto';
import { TitleTranslationDTO } from '../dto/questionnaires/output/title-translation-dto';

@Injectable()
export class SectionsService {

    private sectionsUrl = 'http://109.135.0.135/survey-composer/api/sections';

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * findSections
     * @param includeNested
     */
    findSections(includeNested?: SectionDTO): Observable<SectionDTO[]> {
        const url = `${this.sectionsUrl}${includeNested ? '?includeNested=true' : '?includeNested=false'}`;
        return this.http.get<SectionDTO[]>(url).pipe(
            tap(sections => this.log(`Fetched ${sections.length} sections`)),
            catchError(this.handleError<SectionDTO[]>('findSections'))
        );
    }

    /**
     * findSectionById
     * @param sectionId
     */
    findSectionById(sectionId: number): Observable<SectionDTO> {
        const url = `${this.sectionsUrl}/${sectionId}`;
        return this.http.get<SectionDTO>(url).pipe(
            tap(section => this.log(`Fetched section w/ id=${section.sectionId}`)),
            catchError(this.handleError<SectionDTO>('findSectionById'))
        );
    }

    /**
     * createSection
     * @param input
     */
    createSection(input: CreateSection): Observable<SectionDTO> {
        const url = `${this.sectionsUrl}`;
        return this.http.post<SectionDTO>(url, input).pipe(
            tap(section => {
                this.log(`Created section w/ id=${section.sectionId}`);
            }),
            catchError(this.handleError<SectionDTO>('createSection'))
        );
    }

    /**
     * updateSection
     * @param sectionId
     * @param input
     */
    updateSection(sectionId: number, input: UpdateSection): Observable<SectionDTO> {
        const url = `${this.sectionsUrl}/${sectionId}`;
        return this.http.put<SectionDTO>(url, input).pipe(
            tap(section => this.log(`Updated section w/ id=${section.sectionId}`)),
            catchError(this.handleError<SectionDTO>('updateSection'))
        );
    }


    /**
     * deleteSection
     * @param sectionId
     */
    deleteSection(sectionId: number): Observable<SectionDTO> {
        const url = `${this.sectionsUrl}/${sectionId}`;
        return this.http.delete<SectionDTO>(url).pipe(
            tap(s => this.log(`Updated section w/ id=${s.sectionId}`)),
            catchError(this.handleError<SectionDTO>('deleteSection'))
        );
    }

    /**
     * findTitle
     * @param sectionId
     */
    findTitle(sectionId: number): Observable<TitleDTO> {
        const url = `${this.sectionsUrl}/${sectionId}/titles`;
        return this.http.get<TitleDTO>(url).pipe(
            tap(title => this.log(`Fetched title w/ id=${title.titleId}`)),
            catchError(this.handleError<TitleDTO>('findTitle'))
        );
    }

    /**
     * createTitleTranslation
     * @param sectionId
     * @param CreateTitleTranslation
     */
    createTitleTranslation(sectionId: number, input: CreateTitleTranslation): Observable<TitleTranslationDTO> {
        const url = `${this.sectionsUrl}/${sectionId}/titleTranslations`;
        return this.http.post<TitleTranslationDTO>(url, input).pipe(
            tap(titleTranslation => this.log(`Created translation w/ id=${titleTranslation.titleTranslationId}`)),
            catchError(this.handleError<TitleTranslationDTO>('createTitleTranslation'))
        );
    }

    /**
     * updateTitleTranslation
     * @param sectionId
     * @param titleTranslationId
     * @param input
     */
    updateTitleTranslation(sectionId: number, titleTranslationId: number, input: UpdateTitleTranslation): Observable<TitleTranslationDTO> {
        const url = `${this.sectionsUrl}/${sectionId}/titleTranslations/${titleTranslationId}`;
        return this.http.put<TitleTranslationDTO>(url, input).pipe(
            tap(titleTranslation => this.log(`Updated translation w/ id=${titleTranslation.titleTranslationId}`)),
            catchError(this.handleError<TitleTranslationDTO>('updateTitleTranslation'))
        );
    }

    /**
     * deleteTitleTranslation
     * @param sectionId
     * @param titleTranslationId
     */
    deleteTitleTranslation(sectionId: number, titleTranslationId: number): Observable<TitleTranslationDTO> {
        const url = `${this.sectionsUrl}/${sectionId}/titleTranslations/${titleTranslationId}`;
        return this.http.delete<TitleTranslationDTO>(url).pipe(
            tap(tt => this.log(`Deleted translation w/ id=${tt.titleTranslationId}`)),
            catchError(this.handleError<TitleTranslationDTO>('deleteTitleTranslation'))
        );
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Questions Service : ' + message);
    }
}
