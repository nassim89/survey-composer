import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';
import { CreateAnswer } from '../dto/answers/input/create-answer';
import { UpdateAnswer } from '../dto/answers/input/update-answer';
import { CreateCategory } from '../dto/questions/input/create-category';
import { UpdateCategory } from '../dto/questions/input/update-category';
import { CreateAnswerVersion } from '../dto/answers/input/create-answer-version';
import { UpdateAnswerVersion } from '../dto/answers/input/update-answer-version';
import { CreateAnswerTranslation } from '../dto/answers/input/create-answer-translation';
import { UpdateAnswerTranslation } from '../dto/answers/input/update-answer-translation';
import { ImportationMessage } from '../dto/filesIO/importation-message';
import { AnswerDTO } from '../dto/answers/output/answer-dto';
import { AnswerCategoryDTO } from '../dto/answers/output/answer-category-dto';
import { AnswerVersionDTO } from '../dto/answers/output/answer-version-dto';
import { AnswerTranslationDTO } from '../dto/answers/output/answer-translation-dto';

@Injectable()
export class AnswersService {
    private groupsUrl = 'http://109.135.0.135/survey-composer/api/answers/categories';
    private answersUrl = 'http://109.135.0.135/survey-composer/api/answers';

    constructor(private http: HttpClient, private messageService: MessageService) {}

    /**
     *
     * @param answerCategoryId
     */
    findAnswers(answerCategoryId?: number): Observable<AnswerDTO[]> {
        const url = `${this.answersUrl}`;
        const params = {
            answerCategoryId: answerCategoryId.toString()
        };

        return this.http.get<AnswerDTO[]>(url, { params: params }).pipe(
            tap(answers => this.log(`Fetched ${answers.length} answers.`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerById
     * @param answerId
     */
    findAnswerById(answerId: number): Observable<AnswerDTO> {
        const url = `${this.answersUrl}/${answerId}`;
        return this.http.get<AnswerDTO>(url).pipe(
            tap(answer => this.log(`Fetched answer w/ id = ${answer.answerId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createAnswer
     * @param input
     */
    createAnswer(input: CreateAnswer): Observable<AnswerDTO> {
        const url = `${this.answersUrl}`;
        return this.http.post<AnswerDTO>(url, input).pipe(
            tap(answer => this.log(`Added a answer w/ id : ${answer.answerId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateAnswer
     * @param answerId
     * @param input
     */
    updateAnswer(answerId: number, input: UpdateAnswer): Observable<AnswerDTO> {
        const url = `${this.answersUrl}/${answerId}`;
        return this.http.put<AnswerDTO>(url, input).pipe(
            tap(answer => this.log(`Updated a answer w/ id : ${answer.answerId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteAnswer
     * @param answerId
     */
    deleteAnswer(answerId: number): Observable<AnswerDTO> {
        const url = `${this.answersUrl}/${answerId}`;
        return this.http.delete<AnswerDTO>(url).pipe(
            tap(a => this.log(`Deleted a answer w/ id : ${a.answerId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerCategories
     * @param includeNested
     */
    findAnswerCategories(includeNested?: boolean): Observable<AnswerCategoryDTO[]> {
        const url = `${this.groupsUrl}${
            includeNested ? '?includeNested=true' : '?includeNested=false'
        }`;
        return this.http.get<AnswerCategoryDTO[]>(url).pipe(
            tap(categories => this.log(`Fetched ${categories.length} categories`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerCategoriesById
     * @param answerCategoryId
     */
    findAnswerCategoriesById(answerCategoryId: number): Observable<AnswerCategoryDTO> {
        const url = `${this.groupsUrl}/${answerCategoryId}`;
        return this.http.get<AnswerCategoryDTO>(url).pipe(
            tap(category => this.log(`Fetched category w/ id=${category.answerCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createAnswerCategory
     * @param input
     */
    createAnswerCategory(input: CreateCategory): Observable<AnswerCategoryDTO> {
        const url = `${this.groupsUrl}`;
        return this.http.post<AnswerCategoryDTO>(url, input).pipe(
            tap(category => this.log(`Created category w/ id=${category.answerCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateAnswerCategory
     * @param answerCategoryId
     * @param input
     */
    updateAnswerCategory(
        answerCategoryId: number,
        input: UpdateCategory
    ): Observable<AnswerCategoryDTO> {
        const url = `${this.groupsUrl}/${answerCategoryId}`;
        return this.http.put<AnswerCategoryDTO>(url, input).pipe(
            tap(category => this.log(`Updated category w/ id=${category.answerCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteAnswerCategory
     * @param answerCategoryId
     */
    deleteAnswerCategory(answerCategoryId: number): Observable<AnswerCategoryDTO> {
        const url = `${this.groupsUrl}/${answerCategoryId}`;
        return this.http.delete<AnswerCategoryDTO>(url).pipe(
            tap(ac => this.log(`Deleted category w/ id=${ac.answerCategoryId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerVersions
     * @param answerId
     */
    findAnswerVersions(answerId: number): Observable<AnswerVersionDTO[]> {
        const url = `${this.answersUrl}/${answerId}/versions`;
        return this.http.get<AnswerVersionDTO[]>(url).pipe(
            tap(versions => this.log(`Fetched ${versions.length} versions`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerVersionById
     * @param answerId
     * @param answerVersionId
     */
    findAnswerVersionById(answerId: number, answerVersionId: number): Observable<AnswerVersionDTO> {
        const url = `${this.answersUrl}/${answerId}/versions/${answerVersionId}`;
        return this.http.get<AnswerVersionDTO>(url).pipe(
            tap(version => this.log(`Fetched version w/ id=${version.answerVersionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createVersion
     * @param answerId
     * @param input
     */
    createVersion(answerId: number, input: CreateAnswerVersion): Observable<AnswerVersionDTO> {
        const url = `${this.answersUrl}/${answerId}/versions`;
        return this.http.post<AnswerVersionDTO>(url, input).pipe(
            tap((version: AnswerVersionDTO) =>
                this.log(`Created a version for answer w/ id : ${version.answerVersionId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateAnswerVersion
     * @param answerId
     * @param answerVersionId
     * @param input
     */
    updateAnswerVersion(
        answerId: number,
        answerVersionId: number,
        input: UpdateAnswerVersion
    ): Observable<AnswerVersionDTO> {
        const url = `${this.answersUrl}/${answerId}/versions/${answerVersionId}`;
        return this.http.put<AnswerVersionDTO>(url, input).pipe(
            tap((version: AnswerVersionDTO) =>
                this.log(`Updated a version w/ id : ${version.answerVersionId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteAnswerVersion
     * @param answerId
     * @param answerVersionId
     */
    deleteAnswerVersion(answerId: number, answerVersionId: number): Observable<AnswerVersionDTO> {
        const url = `${this.answersUrl}/${answerId}/versions/${answerVersionId}`;
        return this.http.delete<AnswerVersionDTO>(url).pipe(
            tap(av => this.log(`Deleted a version w/ id : ${av.answerVersionId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerTranslations
     * @param answerId
     * @param answerVersionId
     */
    findAnswerTranslations(
        answerId: number,
        answerVersionId: number
    ): Observable<AnswerTranslationDTO[]> {
        const url = `${this.answersUrl}/${answerId}/versions/${answerVersionId}/translations`;
        return this.http.get<AnswerTranslationDTO[]>(url).pipe(
            tap(translations => this.log(`Fetched ${translations.length} translations`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * findAnswerTanslationById
     * @param answerId
     * @param answerVersionId
     * @param answerTranslationId
     */
    findAnswerTanslationById(
        answerId: number,
        answerVersionId: number,
        answerTranslationId: number
    ): Observable<AnswerTranslationDTO> {
        const url = `${
            this.answersUrl
        }/${answerId}/versions/${answerVersionId}/translations/${answerTranslationId}`;
        return this.http.get<AnswerTranslationDTO>(url).pipe(
            tap(translation =>
                this.log(`Fetched translation w/ id=${translation.answerTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * createAnswerTranslation
     * @param answerId
     * @param answerVersionId
     * @param input
     */
    createAnswerTranslation(
        answerId: number,
        answerVersionId: number,
        input: CreateAnswerTranslation
    ): Observable<AnswerTranslationDTO> {
        const url = `${this.answersUrl}/${answerId}/versions/${answerVersionId}/translations`;
        return this.http.post<AnswerTranslationDTO>(url, input).pipe(
            tap(translation =>
                this.log(`Created translation w/ id=${translation.answerTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * updateAnswerTranslation
     * @param answerId
     * @param answerVersionId
     * @param answerTranslationId
     * @param input
     */
    updateAnswerTranslation(
        answerId: number,
        answerVersionId: number,
        answerTranslationId: number,
        input: UpdateAnswerTranslation
    ): Observable<AnswerTranslationDTO> {
        const url = `${
            this.answersUrl
        }/${answerId}/versions/${answerVersionId}/translations/${answerTranslationId}`;
        return this.http.put<AnswerTranslationDTO>(url, input).pipe(
            tap(translation =>
                this.log(`Updated translation w/ id=${translation.answerTranslationId}`)
            ),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     * deleteAnswerTranslation
     * @param answerId
     * @param answerVersionId
     * @param answerTranslationId
     */
    deleteAnswerTranslation(
        answerId: number,
        answerVersionId: number,
        answerTranslationId: number
    ): Observable<AnswerTranslationDTO> {
        const url = `${
            this.answersUrl
        }/${answerId}/versions/${answerVersionId}/translations/${answerTranslationId}`;
        return this.http.delete<AnswerTranslationDTO>(url).pipe(
            tap(at => this.log(`Deleted translation w/ id=${at.answerTranslationId}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     *
     * @param categoryId
     * @param languageId
     * @param terms
     */
    findAnswerTanslationByFilters(
        versionId?: number[],
        categoryId?: number[],
        languageId?: number[],
        terms?: string[]
    ): Observable<AnswerTranslationDTO[]> {
        let url = `${this.answersUrl}/translations?`;

        if (versionId) {
            const versionIds = versionId.join(',');
            url += `versionId=${versionIds}&`;
        }

        if (categoryId) {
            const categories = categoryId.join(',');
            url += `categoryId=${categories}&`;
        }

        if (languageId) {
            const languages = languageId.join(',');
            url += `languageId=${languages}&`;
        }

        if (terms) {
            const searchTerms = terms.join(',');
            url += `term=${searchTerms}&`;
        }

        return this.http.get<AnswerTranslationDTO[]>(url).pipe(
            tap(translations => this.log(`Fetched ${translations.length} translations`)),
            catchError(e => this.errorHandler(e))
        );
    }

    /**
     *
     * @param fileToUpload
     */
    uploadAnswers(fileToUpload: File): Observable<ImportationMessage> {
        const url = `${this.answersUrl}/file/upload`;
        const formData: FormData = new FormData();
        formData.append('importationFile', fileToUpload, fileToUpload.name);
        return this.http.post<ImportationMessage>(url, formData).pipe(
            tap(b => this.log(`Uploaded answers : ${b}`)),
            catchError(e => this.errorHandler(e))
        );
    }

    downloadAnswers() {
        const url = `${this.answersUrl}/file/download`;
        return this.http.get(url, { responseType: 'blob' as 'json' });
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server error');
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return Observable.throw(error || 'Server error');
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Answers Service : ' + message);
    }
}
