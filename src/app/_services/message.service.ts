import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MessageService {
    messages: string[] = [];
    lastMessage = 'Connecting ...';

    constructor() { }

    add(message: string) {
        this.messages.push(message);
        this.lastMessage = message;
    }

    clear() {
        this.messages = [];
    }

    getLastMessage(): string {
        return this.lastMessage;
    }
}
