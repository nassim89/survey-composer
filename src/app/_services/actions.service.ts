import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { ActionDTO } from '../dto/actions/action-dto';

@Injectable()
export class ActionsService {
    private actionsUrl = 'http://109.135.0.135/survey-composer/api/actions';

    constructor(private http: HttpClient, private messageService: MessageService) {}

    /**
     * findActions
     * @param actionType
     * @param entityId
     * @param entityClass
     * @param userId
     */
    findActions(
        actionType?: number[],
        entityId?: number,
        entityClazz?: string,
        userId?: number
    ): Observable<ActionDTO[]> {
        const url = `${this.actionsUrl}`;
        const params = {
            actionType: JSON.stringify(actionType),
            entityId: entityId.toString(),
            entityClazz: entityClazz,
            userId: userId.toString()
        };

        return this.http.get<ActionDTO[]>(url, { params: params }).pipe(
            tap(actions => this.log(`Fetched ${actions.length} actions`)),
            catchError(this.handleError<ActionDTO[]>('findActions'))
        );
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Questions Service : ' + message);
    }
}
