import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { MessageService } from './message.service';

import { DefaultSettings } from '../dto/settings/input/default-settings';
import { CreateLanguage } from '../dto/settings/input/create-language';
import { UpdateLanguage } from '../dto/settings/input/update-language';
import { CreateCountry } from '../dto/settings/input/create-country';
import { UpdateCountry } from '../dto/settings/input/update-country';
import { DefaultSettingsDTO } from '../dto/settings/output/default-settings-dto';
import { LanguageDTO } from '../dto/settings/output/language-dto';
import { CountryDTO } from '../dto/settings/output/country-dto';

@Injectable()
export class SettingsService {

    private settingsUrl = 'http://109.135.0.135/survey-composer/api/settings';

    constructor(private http: HttpClient, private messageService: MessageService) { }

    /**
     * setDefaultValues
     * @param input
     */
    setDefaultValues(input: DefaultSettings): Observable<DefaultSettingsDTO> {
        const url = `${this.settingsUrl}/default`;
        return this.http.post<DefaultSettingsDTO>(url, input).pipe(
            tap((ds: DefaultSettingsDTO) => this.log(`set the default language`)),
            catchError(this.handleError<DefaultSettingsDTO>('setDefaultValues'))
        );
    }

    /**
     * dropDefaultValues
     */
    dropDefaultValues(): Observable<boolean> {
        const url = `${this.settingsUrl}/default`;
        return this.http.delete<boolean>(url).pipe(
            tap(b => this.log(`dropDefaultValues : ${b}`)),
            catchError(this.handleError<boolean>('dropDefaultValues'))
        );
    }

    /**
     * findLanguages
     */
    findLanguages(): Observable<LanguageDTO[]> {
        const url = `${this.settingsUrl}/languages`;
        return this.http.get<LanguageDTO[]>(url).pipe(
            tap(languages => this.log(`fetched languages`)),
            catchError(this.handleError<LanguageDTO[]>('findLanguages'))
        );
    }

    /**
     * findLanguageById
     * @param languageId
     */
    findLanguageById(languageId: number): Observable<LanguageDTO> {
        const url = `${this.settingsUrl}/languages/${languageId}`;
        return this.http.get<LanguageDTO>(url).pipe(
            tap(language => this.log(`fetched language w\ id ${language.languageId}`)),
            catchError(this.handleError<LanguageDTO>('findLanguageById'))
        );
    }

    /**
     * createLanguage
     * @param input
     */
    createLanguage(input: CreateLanguage): Observable<LanguageDTO> {
        const url = `${this.settingsUrl}/languages`;
        return this.http.post<LanguageDTO>(url, input).pipe(
            tap((l: LanguageDTO) => this.log(`add new language [${l.languageCode}][${l.description}] w\ id ${l.languageId}`)),
            catchError(this.handleError<LanguageDTO>('createLanguage'))
        );
    }

    /**
     * updateLanguage
     * @param languageId
     * @param input
     */
    updateLanguage(languageId: number, input: UpdateLanguage): Observable<LanguageDTO> {
        const url = `${this.settingsUrl}/languages/${languageId}`;
        return this.http.put<LanguageDTO>(url, input).pipe(
            tap((l: LanguageDTO) => this.log(`Updated language [${l.languageCode}][${l.description}] w\ id ${l.languageId}`)),
            catchError(this.handleError<LanguageDTO>('updateLanguage'))
        );
    }

    /**
     * deleteLanguage
     * @param languageId
     */
    deleteLanguage(languageId: number): Observable<LanguageDTO> {
        const url = `${this.settingsUrl}/languages/${languageId}`;
        return this.http.delete<LanguageDTO>(url).pipe(
            tap(l => this.log(`Deleted a language - ${l.languageId}`)),
            catchError(this.handleError<LanguageDTO>('deleteLanguage'))
        );
    }

    /**
     * findCountries
     */
    findCountries(): Observable<CountryDTO[]> {
        const url = `${this.settingsUrl}/countries`;
        return this.http.get<CountryDTO[]>(url).pipe(
            tap(countries => this.log(`Fetched countries`)),
            catchError(this.handleError<CountryDTO[]>('findCountries'))
        );
    }

    /**
     * findCountryById
     * @param countryId
     */
    findCountryById(countryId: number): Observable<CountryDTO> {
        const url = `${this.settingsUrl}/countries/${countryId}`;
        return this.http.get<CountryDTO>(url).pipe(
            tap(country => this.log(`Fetched country`)),
            catchError(this.handleError<CountryDTO>('findCountryById'))
        );
    }

    /**
     * createCoutry
     * @param input
     */
    createCoutry(input: CreateCountry): Observable<CountryDTO> {
        const url = `${this.settingsUrl}/countries`;
        return this.http.post<CountryDTO>(url, input).pipe(
            tap(country => this.log(`Created country`)),
            catchError(this.handleError<CountryDTO>('createCoutry'))
        );
    }

    /**
     * updateCountry
     * @param countryId
     * @param input
     */
    updateCountry(countryId: number, input: UpdateCountry): Observable<CountryDTO> {
        const url = `${this.settingsUrl}/countries/${countryId}`;
        return this.http.put<CountryDTO>(url, input).pipe(
            tap(country => this.log(`Updated country`)),
            catchError(this.handleError<CountryDTO>('updateCountry'))
        );
    }

    /**
     * deleteCountry
     * @param countryId
     */
    deleteCountry(countryId: number): Observable<CountryDTO> {
        const url = `${this.settingsUrl}/countries/${countryId}`;
        return this.http.delete<CountryDTO>(url).pipe(
            tap(c => this.log(`Deleted country`)),
            catchError(this.handleError<CountryDTO>('deleteCountry'))
        );
    }

    private errorHandler(error: HttpErrorResponse) {
        return Observable.throw(error || 'Server error');
    }

    /**
     * handleError
     * @param operation
     * @param result
     */
    private handleError<T>(operation = 'Operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.error.message}`);

            // Let the app keep running by returning an empty result.
            return Observable.throw(error || 'Server error');
        };
    }


    /**
     * log
     * @param message
     */
    private log(message: string) {
        this.messageService.add('Settings Service : ' + message);
    }
}
