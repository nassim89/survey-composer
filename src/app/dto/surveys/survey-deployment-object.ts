import { Questionnaire } from '../../models/questionnaire/questionnaire';
import { AbstractEntity } from '../../models/abstract/abstract-entity';

export class SurveyDeploymentObject extends AbstractEntity {
    surveyDeploymentId: number;
    surveyName: string;
    surveyId: number;
    questionnaire: Questionnaire;
    deploymentResult = false;
    link: string;
    response: string;
}
