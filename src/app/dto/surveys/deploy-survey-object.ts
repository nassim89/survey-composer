import { QuestionnaireDTO } from '../questionnaires/output/questionnaire-dto';

export class DeploySurveyObject {
    questionnaireId: number;
    type = 'survey';
}
