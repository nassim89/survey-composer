export class ErrorMessage {
    line: number;
    message: string;
}
