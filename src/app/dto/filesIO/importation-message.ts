import { ErrorMessage } from './error-message';

export class ImportationMessage {
    description: string;
    errors: ErrorMessage[];
}
