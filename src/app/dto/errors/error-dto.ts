export class ErrorDto {
    message: string;
    status: number;
    exception: string;
    timestamp: Date;
    error: string;
    path: string;
}
