import { FieldErrorDto } from './field-error-dto';

export class ValidationErrorDto {
    fieldErrors: FieldErrorDto[];
}
