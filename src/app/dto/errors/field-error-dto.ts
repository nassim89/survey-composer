export class FieldErrorDto {
    field: string;
    message: string;
}
