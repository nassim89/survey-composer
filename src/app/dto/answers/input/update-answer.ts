/*
    @Min(1)
    private Long categoryId;
    @Size(max=255)
    private String description;
*/

export class UpdateAnswer {
    categoryId: number;
    description: string;

    constructor(categoryId: number, description: string) {
        this.categoryId = categoryId;
        this.description = description;
    }
}
