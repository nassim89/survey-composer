/*
    @Min(1)
    private Long answerId;
    @Size(max=255)
    private String comment;
    @NotNull
    @Size(min=1,max=65535)
    private String text;
*/

export class CreateAnswerVersion {
    answerId: number;
    comment?: string;
    text: string;


    constructor(answerId: number, text: string, comment?: string) {
        this.answerId = answerId;
        this.text = text;
        this.comment = comment;
    }
}

