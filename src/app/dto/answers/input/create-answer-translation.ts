/*
    @Min(1)
    private Long answerVersionId;
    @NotNull
    @Size(min=1,max=65535)
    private String text;
    @NotNull
    @Min(1)
    private Long languageId;
    @Size(max=255)
    private String comment;
*/

export class CreateAnswerTranslation {
    answerVersionId: number;
    text: string;
    languageId: number;
    comment?: string;

    constructor(answerVersionId: number, text: string, languageId: number, comment?: string) {
        this.answerVersionId = answerVersionId;
        this.text = text;
        this.languageId = languageId;
        this.comment = comment;
    }
}
