/*
    @NotNull
    @Size(min=1,max=65535)
    private String text;
    @NotNull
    @Min(1)
    private Long categoryId;
    @Size(max=255)
    private String description;
    @Size(max=255)
    private String comment;
*/

export class CreateAnswer {
    text: string;
    categoryId: number;
    description?: string;
    comment?: string;

    constructor(text: string, categoryId: number, description?: string, comment?: string) {
        this.text = text;
        this.categoryId = categoryId;
        this.description = description;
        this.comment = comment;
    }
}
