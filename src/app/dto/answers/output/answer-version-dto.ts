import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { AnswerTranslationDTO } from './answer-translation-dto';

/*
    private Long answerVersionId;
    private int version;
    Set<AnswerTranslationDTO> translations;
*/

export class AnswerVersionDTO extends AbstractEntityDTO {
    answerVersionId: number;
    version: number;
    translations: AnswerTranslationDTO[];
}
