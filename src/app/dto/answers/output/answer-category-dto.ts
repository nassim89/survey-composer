/*
	private Long answerCategoryId;
	protected String categoryName;
	protected String description;
    private Set<AnswerCategoryDTO> children;
*/

import { AbstractEntityDTO } from '../../abstract-entity-dto';

export class AnswerCategoryDTO extends AbstractEntityDTO {
    answerCategoryId: number;
    categoryName: string;
    description: string;
    children: AnswerCategoryDTO[];
}
