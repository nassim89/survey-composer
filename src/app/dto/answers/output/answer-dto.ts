import { AnswerVersionDTO } from './answer-version-dto';

/*
	private Long answerId;
	private Set<AnswerVersionDTO> versions;
	private String description;
*/

export class AnswerDTO {
    answerId: number;
    versions: AnswerVersionDTO[];
    description: string;
    activeTab: number = 0;
}
