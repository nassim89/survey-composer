import { QuestionVersionDTO } from './question-version-dto';
import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { LabelDTO } from './label-dto';

/*
    private Long questionId;
    private Set<QuestionVersionDTO> versions;
    private Set<LabelDTO> labels;
    private String description;
*/

export class QuestionDTO extends AbstractEntityDTO {
    questionId: number;
    versions: QuestionVersionDTO[];
    labels: LabelDTO[];
    description: string;
    activeTab: number = 0;
}
