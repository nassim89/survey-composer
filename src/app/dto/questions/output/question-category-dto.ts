import { AbstractEntityDTO } from '../../abstract-entity-dto';

/*
    private Long questionCategoryId;
    protected String categoryName;
    protected String description;
    private Set<QuestionCategoryDTO> children;
*/

export class QuestionCategoryDTO extends AbstractEntityDTO {
    questionCategoryId: number;
    categoryName: string;
    description: string;
    children: QuestionCategoryDTO[];
}
