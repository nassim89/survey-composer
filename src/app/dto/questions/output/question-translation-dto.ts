import { AbstractEntityDTO } from '../../abstract-entity-dto';

/*
	private Long questionTranslationId;
	private int translationVersion;
	private String text;
	private String translationComment;
	private String mappingTag ;
	private Long languageId;
	private String languageCode;
	private String languageDescription;
	private Boolean  isDefaultLanguage;
*/

export class QuestionTranslationDTO extends AbstractEntityDTO {
    questionTranslationId: number;
    translationVersion: number;
    text: string;
    translationComment: string;
    mappingTag: string;
    languageId: number;
    languageCode: string;
    languageDescription: string;
    isDefaultLanguage: boolean;
}
