import { AbstractEntityDTO } from "../../abstract-entity-dto";

/*
	private Long labelId;
	private String labelText;
	private String description;
*/

export class LabelDTO extends AbstractEntityDTO {
    labelId: number;
    labelText: string;
    description: string;
}
