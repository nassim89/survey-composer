import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { QuestionTranslationDTO } from './question-translation-dto';

/*
    private Long questionVersionId;
    private int version;
    Set<QuestionTranslationDTO> translations;
*/

export class QuestionVersionDTO extends AbstractEntityDTO {
    questionVersionId: number;
    version: number;
    translations: QuestionTranslationDTO[];
}
