/*
	@Min(1)
	private Long categoryId;
	@Size(max=255)
	private String description;
	private List<Long> labelIds;
*/

export class UpdateQuestion {
    categoryId: number;
    description: string;
    labelIds: number[];

    constructor(categoryId: number, description: string, labelIds: number[]) {
        this.categoryId = categoryId;
        this.description = description;
        this.labelIds = labelIds;
    }
}
