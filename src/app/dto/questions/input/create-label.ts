/*
	@NotNull
	@Size(max=255)
	private String label;
	@Size(max=255)
	private String description;
*/

export class CreateLabel {
    label: string;
    description?: string;

    constructor(label: string, description?: string) {
        this.label = label;
        this.description = description;
    }
}
