/*
    @Size(min=1,max=65535)
    private String text;
    @Min(1)
    private Long languageId;
    @Size(max=255)
    private String comment;
*/

export class UpdateQuestionTranslation {
    text: string;
    languageId: number;
    comment: string;

    constructor(text: string, languageId: number, comment: string) {
        this.text = text;
        this.languageId = languageId;
        this.comment = comment;
    }
}
