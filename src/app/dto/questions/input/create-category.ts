/*
	@NotNull
	@Size(max=255)
	private String name;
	@Size(max=255)
	private String description;
	@Min(1)
    private Long parentId;
*/

export class CreateCategory {
    name: string;
    description?: string;
    parentId?: number;

    constructor(name: string, description?: string, parentId?: number) {
        this.name = name;
        this.description = description;
        this.parentId = parentId;
    }
}
