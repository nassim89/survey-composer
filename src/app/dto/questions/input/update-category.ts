/*
    @Size(max=255)
    private String name;
    @Size(max=255)
    private String description;
*/

export class UpdateCategory {
    name: string;
    description: string;

    constructor(name: string, description: string) {
        this.name = name;
        this.description = description;
    }
}
