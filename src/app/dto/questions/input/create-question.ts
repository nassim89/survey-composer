/*
	@NotNull
	@Size(min=1,max=65535)
	private String text;
	@NotNull
	@Min(1)
	private Long categoryId;
	@Size(max=255)
	private String description;
	@Size(max=255)
	private String comment;
	private List<Long> labelIds;
*/

export class CreateQuestion {
    text: string;
    categoryId: number;
    description?: string;
    comment?: string;
    labelIds?: number[];

    constructor(text: string, categoryId: number, description?: string, comment?: string, labelIds?: number[]) {
        this.text = text;
        this.categoryId = categoryId;
        this.description = description;
        this.comment = comment;
        this.labelIds = labelIds;
    }
}
