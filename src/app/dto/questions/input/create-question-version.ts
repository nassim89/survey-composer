/*
    @Min(1)
    private Long questionId;

    @Size(max=255)
    private String comment;

    @NotNull
    @Size(min=1,max=65535)
    private String text;
*/

export class CreateQuestionVersion {
    text: string;
    questionId: number;
    comment?: string;

    constructor(questionId: number, text: string, comment?: string) {
        this.text = text;
        this.questionId = questionId;
        this.comment = comment;
    }
}
