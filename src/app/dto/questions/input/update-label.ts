/*
    @Size(max=255)
    private String label;
    @Size(max=255)
    private String description;
*/

export class UpdateLabel {
    label: string;
    description: string;

    constructor(label: string, description: string) {
        this.label = label;
        this.description = description;
    }
}
