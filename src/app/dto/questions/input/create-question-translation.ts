/*
	@Min(1)
	private Long questionVersionId;
	@NotNull
	@Size(min=1,max=65535)
	private String text;
	@NotNull
	@Min(1)
	private Long languageId;
	@Size(max=255)
	private String comment;
*/

export class CreateQuestionTranslation {
    questionVersionId: number;
    text: string;
    languageId: number;
    comment?: string;

    constructor(questionVersionId: number, text: string, languageId: number, comment?: string) {
        this.questionVersionId = questionVersionId;
        this.text = text;
        this.languageId = languageId;
        this.comment = comment;
    }
}
