/*
	protected Date creationDate;
	protected Date lastModificationDate;
	protected Boolean isDeleted=false;
	protected Date deletedOn;
    protected List<ActionDTO> actions;
*/

import { ActionDTO } from './actions/action-dto';

export abstract class AbstractEntityDTO {
    creationDate: Date;
    lastModification: Date;
    isDeleted: Boolean;
    deletedOn: Date;
    actions: ActionDTO[];
}
