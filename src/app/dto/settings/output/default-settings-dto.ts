/*
	private Language defaultLanguage;
*/

import { LanguageDTO } from './language-dto';

export class DefaultSettingsDTO {
    defaultLanguage: LanguageDTO;
}
