/*
    private Long languageId;
    private String languageCode;
    private Boolean isDefault;
*/

import { AbstractEntityDTO } from '../../abstract-entity-dto';

export class LanguageDTO extends AbstractEntityDTO {
    languageId: number;
    languageCode: string;
    description: string;
    isDefault: boolean;
}
