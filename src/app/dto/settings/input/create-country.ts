/*
	@NotNull
	@Size(min=2)
	private String coutryCode;
	@Size(max=255)
	private String description;
*/

export class CreateCountry {
    countryCode: string;
    description?: string;

    constructor(countryCode: string, description?: string) {
        this.countryCode = countryCode;
        this.description = description;
    }
}
