/*
	@NotNull
	@Size(min=2)
	private String languageCode;
	@Size(max=255)
	private String description;
*/

export class DefaultSettings {
    languageCode: string;
    description?: string;

    constructor(languageCode: string, description?: string) {
        this.languageCode = languageCode;
        this.description = description;
    }
}
