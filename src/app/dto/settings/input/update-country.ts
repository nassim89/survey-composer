/*
	@Size(min=2)
	private String coutryCode;
	@Size(max=2^8)
	private String description;
*/

export class UpdateCountry {
    coutryCode: string;
    description?: string;

    constructor(countryCode: string, description?: string) {
        this.coutryCode = countryCode;
        this.description = description;
    }
}
