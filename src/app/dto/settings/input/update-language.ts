/*
	@NotNull
	@Size(min=2)
	private String coutryCode;
	@Size(max=255)
	private String description;
*/

export class UpdateLanguage {
    languageCode: string;
    description?: string;

    constructor(languageCode: string, description?: string) {
        this.languageCode = languageCode;
        this.description = description;
    }
}
