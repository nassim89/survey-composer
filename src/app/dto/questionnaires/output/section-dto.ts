import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { TitleDTO } from './title-dto';
import { QAMapDTO } from './qamap-dto';
import { QuestionParameter } from '../../../models/questionnaire/question-parameter';
import { AnswerParameter } from '../../../models/questionnaire/answer-parameter';

/*
    private Long sectionId;
    private TitleDTO sectionTitle;
    private Set<SectionDTO> children;
    private OptionsDisplayType displayType;
    private QAMapDTO map;
*/

export class SectionDTO extends AbstractEntityDTO {
    sectionId: number;
    sectionTitle: TitleDTO;
    children: SectionDTO[];
    displayType: number;
    map: QAMapDTO;
    questionParameter: QuestionParameter;
    answerParameter: AnswerParameter;
}
