/*
    private Long questionnaireGroupId;
    private String groupName;
    private String description;
*/

import { AbstractEntityDTO } from '../../abstract-entity-dto';

export class QuestionnaireGroupDTO extends AbstractEntityDTO {
    questionnaireGroupId: number;
    groupName: string;
    description: string;
}
