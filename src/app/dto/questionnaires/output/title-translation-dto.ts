import { AbstractEntityDTO } from '../../abstract-entity-dto';

/*
	private Long titleTranslationId;
	private String titleText;
	private Long languageId;
	private String languageCode;
	private Boolean isDefaultLanguage;
*/

export class TitleTranslationDTO extends AbstractEntityDTO {
    titleTranslationId: number;
    titleText: string;
    languageId: number;
    languageCode: string;
    isDefaultLanguage: boolean;
}
