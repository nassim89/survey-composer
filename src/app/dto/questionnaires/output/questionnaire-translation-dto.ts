import { LanguageDTO } from '../../settings/output/language-dto';

export class QuestionnaireTranslationDTO {
    questionTranslationId: number;
    translationVersion: number;
    text: string;
    translationComment: string;
    mappingTag: string;
    language: LanguageDTO;

    constructor(
        questionTranslationId: number, translationVersion: number, text: string,
        translationComment: string, mappingTag: string, language: LanguageDTO
    ) {
        this.questionTranslationId = questionTranslationId;
        this.translationVersion = translationVersion;
        this.text = text;
        this.translationComment = translationComment;
        this.mappingTag = mappingTag;
        this.language = language;
    }
}
