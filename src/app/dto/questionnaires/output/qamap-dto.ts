import { QuestionTranslationDTO } from '../../questions/output/question-translation-dto';
import { AnswerTranslationDTO } from '../../answers/output/answer-translation-dto';

/*
	private QuestionTranslationDTO questionTranslation;
	//private Long qaMapId;
	private List<AnswerTranslationDTO>  answerTranslations;
*/

export class QAMapDTO {
    questionTranslation: QuestionTranslationDTO;
    answerTranslations: AnswerTranslationDTO[];
}
