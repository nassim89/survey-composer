import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { TitleDTO } from './title-dto';
import { SectionDTO } from './section-dto';

/*
	private long questionnaireId;
	private TitleDTO title;
	private Long languageId;
	private String languageCode;
	private String languageDescription;
	private Long questionnaireTranslationGroupId;
	private Set<SectionDTO> sections;

*/

export class QuestionnaireDTO extends AbstractEntityDTO {
    questionnaireId: number;
    title: TitleDTO;
    languageId: number;
    languageCode: string;
    languageDescription: string;
    questionnaireTranslationGroupId: number;
    sections: SectionDTO[];
}
