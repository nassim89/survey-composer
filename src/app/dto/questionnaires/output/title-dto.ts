import { TitleTranslationDTO } from './title-translation-dto';
import { AbstractEntityDTO } from '../../abstract-entity-dto';

/*
	private Long titleId;
	private String defaultTitleText;
	private Set<TitleTranslationDTO> titleTranslations;
*/

export class TitleDTO extends AbstractEntityDTO {
    titleId: number;
    defaultTitleText: string;
    titleTranslations: TitleTranslationDTO[];
}
