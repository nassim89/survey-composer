export class UpdateQAMap {
    mapId: number;
    questionId: number;
    answerId: number;

    constructor(mapId: number, questionId: number, answerId: number) {
        this.mapId = mapId;
        this.questionId = questionId;
        this.answerId = answerId;
    }
}
