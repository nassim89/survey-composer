export class CreateQuestionParameter {
    qpOrientation = 'HORZ';
    qpLabelsRight = false;
    qpQuestionDescriptionAbove = false;
    qpCustomCSS: string;
    qpForceNumeric = false;
    qpForcePercent = false;
    qpForceCurrency = false;
    qpMinNumber = 0;
    qpMaxNumber = 0;
    qpMinAnswerPerRow = 0;
    qpMinimumResponse = 0;
}
