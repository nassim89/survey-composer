export class CreateQuestionnaire {
    questionnaireTitle: string;
    sectionIds: number[];
    questionnaireTranslationGroupId?: number;
    languageId = 1;

    constructor(questionnaireTitle: string, sectionIds?: number[], languageId?: number, questionnaireTranslationGroupId?: number) {
        this.questionnaireTitle = questionnaireTitle;
        this.sectionIds = sectionIds;
        this.languageId = languageId;
        this.questionnaireTranslationGroupId = questionnaireTranslationGroupId;
    }
}
