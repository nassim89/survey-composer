export class CreateAnswerParameter {
    apOther: boolean;
    apRequireOther: boolean;
    apNa: boolean;
    apNone: boolean;
    apAll: boolean;
    apFixed: boolean;
}
