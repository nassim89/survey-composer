import { CreateQAMap } from './create-qamap';
import { QA } from '../../../models/local/qa';
import { OptionDisplayType } from '../../../models/enums/display-type.enum';
import { CreateQuestionParameter } from './create-question-parameter';
import { CreateAnswerParameter } from './create-answer-parameter';
import { QuestionTranslationDTO } from '../../questions/output/question-translation-dto';
import { AnswerTranslationDTO } from '../../answers/output/answer-translation-dto';

export class CreateSection {
    title: string;
    displayType: OptionDisplayType;
    questionParameter: CreateQuestionParameter;
    answerParameter: CreateAnswerParameter;
    sectionNumber?: number;
    maps?: CreateQAMap[];
    children?: CreateSection[];
    qa?: QA;

    constructor(
        title: string,
        odt: OptionDisplayType,
        questionParameter?: CreateQuestionParameter,
        answerParameter?: CreateAnswerParameter,
        sectionNumber?: number,
        maps?: CreateQAMap[],
        children?: CreateSection[]
    ) {
        this.title = title;
        this.maps = maps;
        this.displayType = odt;
        this.questionParameter = questionParameter;
        this.answerParameter = answerParameter;
        this.sectionNumber = sectionNumber;
        this.qa = new QA();
        this.children = children;
    }
}
