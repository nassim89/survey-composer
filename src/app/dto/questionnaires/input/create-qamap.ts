export class CreateQAMap {
    questionId: number;
    answerId: number;

    constructor(questionId: number, answerId?: number) {
        this.questionId = questionId;
        this.answerId = answerId;
    }
}
