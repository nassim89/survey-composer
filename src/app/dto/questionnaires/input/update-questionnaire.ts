export class UpdateQuestionnaire {
    questionnaireTitle: string;
    sectionIds: number[];
    questionnaireGroupId?: number;

    constructor(questionnaireTitle: string, sectionIds: number[], questionnaireGroupId?: number) {
        this.questionnaireTitle = questionnaireTitle;
        this.sectionIds = sectionIds;
        this.questionnaireGroupId = questionnaireGroupId;
    }
}
