import { UpdateQAMap } from './update-qamap';

export class UpdateSection {
    title: string;
    maps: UpdateQAMap[];
    children: UpdateSection[];

    constructor(title: string, maps: UpdateQAMap[], children: UpdateSection[]) {
        this.title = title;
        this.maps = maps;
        this.children = children;
    }
}
