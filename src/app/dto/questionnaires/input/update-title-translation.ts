export class UpdateTitleTranslation {
    title: string;
    languageId: number;

    constructor(title: string, languageId: number) {
        this.title = title;
        this.languageId = languageId;
    }
}
