export class CreateTitleTranslation {
    title: string;
    languageId: number;

    constructor(title: string, languageId: number) {
        this.title = title;
        this.languageId = languageId;
    }
}
