/*
	@NotNull
	@Size(max=255)
	private String firstName;
	@Size(max=255)
	private String lastName;
	@NotNull
	@Size(max=255)
	private String userName;
	@NotNull
	@Size(min=1,max=255)
	private String password;
	@NotNull
	@Email
	private String email;
	private List<Long> roleIds;
*/

export class CreateUser {
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    roleIds: number[];

    constructor(firstName: string, lastName: string, userName: string, password: string, email: string, roleIds: number[]) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.roleIds = roleIds;
    }
}
