/*
    @Size(max=255)
    private String firstName;
    @Size(max=255)
    private String lastName;
    @Size(max=255)
    private String userName;
    @Size(min=1,max=255)
    private String password;
    @Email
    private String email;
    private List<Long> roles;
*/

export class UpdateUser {
    firstName: string;
    lastName: string;
    userName: string;
    password: string;
    email: string;
    roleIds: number[];

    constructor(firstName: string, lastName: string, userName: string, password: string, email: string, roleIds: number[]) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.roleIds = roleIds;
    }
}
