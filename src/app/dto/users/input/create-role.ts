/*
    @NotNull
    private String name;
    @NotNull
    @NotEmpty
    List<Long> permissionIds;
*/

export class CreateRole {
    name: string;
    permissionIds: number[];

    constructor(name: string, permissionIds: number[]) {
        this.name = name;
        this.permissionIds = permissionIds;
    }
}
