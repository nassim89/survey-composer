/*
private String name;
List<Long> permissionIds;
*/

export class UpdateRole {
    name: string;
    permissionIds: number[];

    constructor(name: string, permissionIds: number[]) {
        this.name = name;
        this.permissionIds = permissionIds;
    }
}
