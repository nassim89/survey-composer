import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { PermissionEntityDTO } from './permission-entity-dto';

/*
	private Long userRoleId;
	private String roleName;
	private Set<PermissionEntityDTO> permissions;
*/

export class UserRoleDTO extends AbstractEntityDTO {
    userRoleId: number;
    roleName: string;
    permissions: PermissionEntityDTO[];
}
