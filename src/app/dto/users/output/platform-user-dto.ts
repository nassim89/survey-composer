import { AbstractEntityDTO } from '../../abstract-entity-dto';
import { UserRoleDTO } from './user-role-dto';

/*
    private Long userId;
    private String firstName;
    private String lastName;
    private String userName;
    private String password;
    private String email;
    private Set<UserRoleDTO> roles;
*/

export class PlatformUserDTO extends AbstractEntityDTO {
    userId: number;
    firstName: string;
    lastName: string;
    userName: string;
    accessToken: string;
    email: string;
    roles: UserRoleDTO[];
}
