import { AbstractEntityDTO } from '../../abstract-entity-dto';

/*
	private Long permissionId;
	private String name;
	private String description;
*/

export class PermissionEntityDTO extends AbstractEntityDTO {
    permissionId: number;
    name: string;
    description: string;
}
