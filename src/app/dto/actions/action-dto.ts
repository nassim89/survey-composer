/*
    private Long actionId;
    private Integer actionType;
    private Date dateOfAction;
    private Long entityId;
    private String entityClazz;
    private Long userId;
    private String userName;
    private String userEmail;
*/

export class ActionDTO {
    actionId: number;
    actionType: number;
    dateOfAction: Date;
    entityId: number;
    entityClazz: string;
    userId: number;
    userName: string;
    userEmail: string;
}
